/*
* Licensed under the MIT license:
*
* Copyright (c) 2020 Manuel Streicher

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*
*/

#include "raw_osm_network.h"

namespace tuk::osg::rawNetwork {

    RawOsmNetwork::RawOsmNetwork() {

    }

    RawWay &RawOsmNetwork::addWay(id_type id) {
        return _ways.insert(std::make_pair(id, RawWay(id))).first->second;
    }

    RawNode &RawOsmNetwork::addNode(id_type id, coord_t lat, coord_t lon) {
        return _nodes.insert(std::make_pair(id, RawNode(id, lat, lon))).first->second;
    }

    RawRestriction &RawOsmNetwork::addRestriction(id_type id, RestrictionType type) {
        return _restrictions.insert(std::make_pair(id, RawRestriction(id, type))).first->second;
    }

    bool RawOsmNetwork::containsRestriction(id_type id) {
        return _restrictions.find(id) != _restrictions.end();
    }

    bool RawOsmNetwork::containsWay(id_type id) {
        return _ways.find(id) != _ways.end();
    }

    bool RawOsmNetwork::containsNode(id_type id) {
        return _nodes.find(id) != _nodes.end();
    }
    
    int RawOsmNetwork::number_nodes() const {
        return _nodes.size(); 
    }

    int RawOsmNetwork::number_ways() const {
        return _ways.size();
    }

    int RawOsmNetwork::number_restrictions() const {
        return _restrictions.size();
    }

    RawRestriction &RawOsmNetwork::getRestriction(id_type id) {
        return _restrictions.at(id);
    }

    RawWay &RawOsmNetwork::getWay(id_type id) {
        return _ways.at(id);
    }

    RawNode &RawOsmNetwork::getNode(id_type id) {
        return _nodes.at(id);
    }

}
