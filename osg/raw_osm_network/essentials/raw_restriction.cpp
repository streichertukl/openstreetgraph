/*
* Licensed under the MIT license:
*
* Copyright (c) 2020 Manuel Streicher

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*
*/

#include "raw_restriction.h"

namespace tuk::osg::rawNetwork
{
    RawRestriction::RawRestriction (id_type id, RestrictionType type)
    : RawElement (id)
    , _type (type)
    {}

    void RawRestriction::add_member (id_type id, OsmElement type, members_t &members)
    {
        switch(members.first)
        {
            case NO_ELEMENT:
                members.first = type;
                members.second.push_back(id);
                break;
            default:
                if (members.first != type) throw std::invalid_argument("Different membertypes in Restriction");
                members.second.push_front(id);
        }
    }

    std::set<id_type> RawRestriction::contained_ids (OsmElement type) const
    {
      std::set<id_type> s;
      if (type == _from.first)
      {
        s.insert (_from.second.begin(), _from.second.end());
      }
      if (type == _via.first)
      {
        s.insert (_via.second.begin(), _via.second.end());
      }
      if (type == _to.first)
      {
        s.insert (_to.second.begin(), _to.second.end());
      }
      return s;
    }

    bool RawRestriction::has_from_element (OsmElement type, id_type id) const
    {
      if (type != _from.first)
      {
        return false;
      }
      for (auto& x : _from.second)
      {
        if (id == x)
        {
          return true;
        }
      }
      return false;
    }

    bool RawRestriction::has_via_element (OsmElement type, id_type id) const
    {
      if (type != _via.first)
      {
        return false;
      }
      for (auto& x : _via.second)
      {
        if (id == x)
        {
          return true;
        }
      }
      return false;
    }

    bool RawRestriction::has_to_element (OsmElement type, id_type id) const
    {
      if (type != _to.first)
      {
        return false;
      }
      for (auto& x : _to.second)
      {
        if (id == x)
        {
          return true;
        }
      }
      return false;
    }
}
