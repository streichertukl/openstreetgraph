/*
* Licensed under the MIT license:
*
* Copyright (c) 2020 Manuel Streicher

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*
*/

#pragma once

#include <forward_list>

#include "raw_element.h"

namespace tuk::osg::rawNetwork
{
    class RawNode : public RawElement {
    public:
        RawNode(id_type id, coord_t lat, coord_t lon);

        OsmElement getType() {return NODE;}

        // Getter:
        std::pair<coord_t,coord_t> coord() const {return std::pair<coord_t, coord_t>(_lon, _lat);};
        coord_t lat() const {return _lat;}
        coord_t lon() const {return _lon;}

        // Add a restriction corresponding to this node
        void addRestriction(id_type id) {_restrictions.push_front(id);}

        void addWay(int numberOfExits, bool isOneway);

        const std::forward_list<id_type>& restrictions() const {return _restrictions;}

        int numberOfExits() {return _numberOfExits;};
        int incidentStreets() {return _incidentStreets;};
        bool isDeadend() {return _deadend;};
    protected:
        coord_t _lat;
        coord_t _lon;
        std::forward_list<id_type> _restrictions;
        int _numberOfExits {0};
        int _incidentStreets {0};
        bool _deadend {false};
    };
}
