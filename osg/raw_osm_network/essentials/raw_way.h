/*
* Licensed under the MIT license:
*
* Copyright (c) 2020 Manuel Streicher

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*
*/
#pragma once

#include <iostream>
#include <list>
#include <utility>

#include "../../config.h"
#include "raw_element.h"
#include "raw_node.h"

namespace tuk::osg::rawNetwork
{
    class RawWay : public RawElement{
    public:
        //Constructors:
        explicit RawWay(id_type);

        OsmElement getType() {return WAY;}

        void addWaypoints(std::list<id_type> &&waypoints);

        //Adding a restriction corresponding to the way
        void addRestriction(id_type id) {_restrictions.push_front(id);}
        bool setOnewayType();

        bool oneway() {return _oneway;};

        const std::list<id_type> &waypoints() const {return _waypoints;}
        const std::forward_list<id_type> &restrictions() const {return _restrictions;}

    protected:
        std::list<id_type> _waypoints;
        std::forward_list<id_type> _restrictions;
        bool _oneway {false};
    };
}
