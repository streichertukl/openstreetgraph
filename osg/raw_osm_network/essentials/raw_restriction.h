//
/*
* Licensed under the MIT license:
*
* Copyright (c) 2020 Manuel Streicher

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*
*/

#pragma once

#include <set>

#include "../../config.h"
#include "../../constants.h"
#include "raw_element.h"
#include "raw_node.h"

namespace tuk::osg::rawNetwork
{
    using members_t = std::pair<OsmElement, std::list<id_type>>;

    class RawRestriction : public RawElement{
    public:
        RawRestriction(id_type id, RestrictionType type);

        OsmElement getType() {return RELATION;}

        void add_from(id_type id, OsmElement type) {add_member(id, type, _from);}
        void add_to(id_type id, OsmElement type) {add_member(id, type, _to);}
        void add_via(id_type id, OsmElement type) {add_member(id, type, _via);}
        static void add_member(id_type id, OsmElement type, members_t &members);
        
        OsmElement osmelement_from() const {return _from.first;}
        OsmElement osmelement_via() const {return _via.first;}
        OsmElement osmelement_to() const {return _to.first;}

        const std::list<id_type> & from_ids() const {return _from.second;}
        std::list<id_type> & via_ids() {return _via.second;}
        const std::list<id_type> & to_ids() const {return _to.second;}

        std::set<id_type> contained_ids (OsmElement) const;

        bool has_from_element (OsmElement type, id_type id) const;
        bool has_via_element (OsmElement type, id_type id) const;
        bool has_to_element (OsmElement type, id_type id) const;

        RestrictionType type() {return _type;}
        bool via_reverse() {return _via_reverse;};
        void set_via_reverse(bool reverse) { _via_reverse = reverse;};

    protected:
        RestrictionType _type;
        members_t _from {NO_ELEMENT, std::list<id_type>()};
        members_t _via {NO_ELEMENT, std::list<id_type>()};
        members_t _to {NO_ELEMENT, std::list<id_type>()};
        bool _via_reverse {false};
    };
}
