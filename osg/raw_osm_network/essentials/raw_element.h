/*
* Licensed under the MIT license:
*
* Copyright (c) 2020 Manuel Streicher

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*
*/

#pragma once

#include <iostream>

#include "../../config.h"
#include "../../constants.h"

namespace tuk::osg::rawNetwork {
    class RawElement {
    protected:
        //Constructor:
        explicit RawElement(id_type id);
    public:

        virtual OsmElement getType() = 0;

        //Getter:
        id_type id() const {return _id;};
        std::string getTag(const std::string &key)
        {
            return _tags[key];
        };

        bool containsTag(const std::string &key) const
        {
            return _tags.find(key) != _tags.end();
        };
        // Adding tags:
        void addTag(const std::string& key, std::string value);
        void setTags(tag_map_t tagMap);

        tag_map_t & tags() {return _tags;}
        //Tag Iterators:
        tag_map_t::const_iterator beginTags() {return _tags.begin();}
        tag_map_t::const_iterator endTags() {return _tags.end();}

    protected:
        id_type _id;
        tag_map_t _tags;
    };
}
