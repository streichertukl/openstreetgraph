//
// Created by mese0 on 05.01.2021.
//

#include "components.h"

namespace tuk::osg {
    std::list<std::list<std::weak_ptr<osgraph::OSNode>>> StrongConnectionAlgorithm::run() {
        stack.clear();
        components.clear();
        low.clear();
        number.clear();
        i = 0;
        recurse = false;
        for (auto &node : graph.nodes()) {
            if (number.find(node.second->id()) == number.end()) {
                if (!stack.empty()) {
                    std::cout << "Starting to explore a node without an empty stack!" << std::endl;
                }
                startnum = i;
                strong_connect(node.second);
            }
        }
        return std::move(components);

    }

    void StrongConnectionAlgorithm::strong_connect(std::shared_ptr<osgraph::OSNode> &node) {
        std::forward_list<std::pair<std::weak_ptr<osgraph::OSNode>, std::list<std::weak_ptr<osgraph::OSStreet>>::iterator>> recStack;
        recStack.push_front({node, node->outgoing_streets().begin()});
        std::unordered_set<id_type> onStack;
        while (!recStack.empty()) {
            auto[v, next_neighbor] = recStack.front();
            recStack.pop_front();
            if (number.find(v.lock()->id()) == number.end()) {
                low.insert({v.lock()->id(), i});
                number.insert({v.lock()->id(), i});
                ++i;
                stack.push_front(v);
                onStack.insert(v.lock()->id());
            }
            recurse = false;
            for (auto next = next_neighbor; next != v.lock()->outgoing_streets().end(); ++next) {
                std::shared_ptr<osgraph::OSNode> &w = graph.nodes().find(
                        next->lock()->start() == *(v.lock()) ? next->lock()->end().id()
                                                             : next->lock()->start().id())->second;
                if (*(v.lock()) == *w) continue;
                if (number.find(w->id()) == number.end()) {
                    recStack.push_front({v, std::next(next)});
                    recStack.push_front({w, w->outgoing_streets().begin()});
                    recurse = true;
                    break;
                } else if (onStack.find(w->id()) != onStack.end()) {
                    low[v.lock()->id()] = std::min(low[v.lock()->id()], number[w->id()]);
                }
            }
            if (recurse) continue;
            if (low[v.lock()->id()] == number[v.lock()->id()]) {
                std::list<std::weak_ptr<osgraph::OSNode>> component;
                while (stack.front().lock()->id() != v.lock()->id()) {
                    component.push_back(stack.front());
                    onStack.erase(stack.front().lock()->id());
                    stack.pop_front();
                }
                component.push_back(stack.front());
                onStack.erase(stack.front().lock()->id());
                stack.pop_front();
                components.push_back(component);
            }
            if (!recStack.empty()) {
                low[(recStack.front()).first.lock()->id()] = std::min(low[(recStack.front()).first.lock()->id()],
                                                                      low[v.lock()->id()]);
            }

        }
    }
}
