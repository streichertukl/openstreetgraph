/*
* Licensed under the MIT license:
*
* Copyright (c) 2020 Manuel Streicher

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*
*/

#ifndef TUK_OSG_COMPONENTS_H
#define TUK_OSG_COMPONENTS_H

#include <algorithm>
#include "../config.h"
#include "../osgraph/node.h"
#include "../osgraph/osgraph.h"

namespace tuk::osg {
    class StrongConnectionAlgorithm {
    public:
        explicit StrongConnectionAlgorithm(osgraph::OSGraph &graph) : graph(graph) {}
        std::list<std::list<std::weak_ptr<osgraph::OSNode>>> run();
    private:
        osgraph::OSGraph &graph;
        int i {0};
        int startnum {0};
        bool recurse {false};
        std::unordered_map<id_type, int> number;
        std::unordered_map<id_type, int> low;
        std::forward_list<std::weak_ptr<osgraph::OSNode>> stack;
        std::list<std::list<std::weak_ptr<osgraph::OSNode>>> components;

        void strong_connect(std::shared_ptr<osgraph::OSNode>& node);

    };
}
#endif //TUK_OSG_COMPONENTS_H
