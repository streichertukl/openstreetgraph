/*
* Licensed under the MIT license:
*
* Copyright (c) 2020 Manuel Streicher

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*
*/

#include "street.h"
namespace tuk::osg::osgraph {
    OSStreet::OSStreet(id_type id, std::unordered_set<id_type> &&origins,
                                          tuk::osg::osgraph::OSNode &start,
                                          tuk::osg::osgraph::OSNode &end, tag_map_t &&tags, waypoints_t &&waypoints,
                                          bool oneway,
                                          double length_meter) : OSGElement(id),
                                                                 _origin(std::move(origins)),
                                                                 _start(start),
                                                                 _end(end),
                                                                 _tags(std::move(tags)),
                                                                 _waypoints(std::move(waypoints)),
                                                                 _oneway(oneway),
                                                                 _length_meter(length_meter) {
    }

    std::string OSStreet::getTag(const std::string &key) {
        return _tags[key];
    }

    int OSStreet::maxspeed() {
        try {
            return std::stoi(getTag(MAXSPEED));
        } catch (std::invalid_argument &) {
            return NO_MAX_SPEED;
        }
    }

    int OSStreet::lanes() {
        try {
            return std::stoi(getTag(LANES));
        } catch (std::invalid_argument &) {
            return LANE_STANDARD;
        }
    }

    void OSStreet::addWaypointBack(tuk::osg::coord_pair_t coord) {
        _waypoints.push_back(coord);
    }

    void OSStreet::addWaypointFront(tuk::osg::coord_pair_t coord) {
        _waypoints.push_front(coord);
    }
}