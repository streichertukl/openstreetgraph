/*
* Licensed under the MIT license:
*
* Copyright (c) 2020 Manuel Streicher

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*
*/

#include "osgraph.h"
#include "../tools/distances.h"
#include <iomanip>

namespace tuk::osg::osgraph {
    OSGraph::OSGraph() = default;

    OSGraph::OSGraph(rawNetwork::RawOsmNetwork &raw_network, std::list<std::array<std::string, 2>> &kvNodePairs) {
        way_streets_map_t<std::shared_ptr<OSStreet>> restricted_way_streets{};
        for (const auto & elt : kvNodePairs) {
            _kvNodePairs[elt[0]].push_back(elt[1]);
        }
        for (auto &wayPair : raw_network.ways()) {
            //Streets are objects corresponding to parts of ways from crossing (or endpoint of way) to crossing
            auto corresponding_streets = _splitOsmWayIntoStreetparts(wayPair.second, raw_network);
            if (!wayPair.second.restrictions().empty()) {
                restricted_way_streets.emplace(wayPair.first, std::move(corresponding_streets));
            }
        }

        _logRestrictionProcess = false; // If set to true -> print reason for each unhandled restriction!

        _handle_restrictions(raw_network, restricted_way_streets);
    }

    bool OSGraph::nodeContainedInPolys(std::list<std::list<coord_pair_t>> &polygons, OSNode& node) {
        for (auto & poly : polygons) {
            coord_pair_t lastCoord {poly.back()};
            bool start {true};
            bool even {true};
            coord_t xstar = node.lon();
            coord_t ystar = node.lat();
            coord_t x1, y1, x2, y2;
            for (auto & coord : poly) {
                x1 = lastCoord[0];
                y1 = lastCoord[1];
                x2 = coord[0];
                y2 = coord[1];
                if (((y1 > ystar) != (y2 >= ystar)) && xstar >= (ystar - y1) * (x2 - x1) / (y2 - y1) + x1) {
                    even = !even;
                }
                lastCoord = coord;
            }
            if (!even) return true;
        }
        return false;
    }

    void OSGraph::remove_streets_outside_bounds(std::list<std::list<coord_pair_t>> &polygons) {
        std::forward_list<std::weak_ptr<OSNode>> toBeDeleted;
        for (auto & node_pair : _osnodes) {
            if (!nodeContainedInPolys(polygons, *(node_pair.second))) {
                toBeDeleted.push_front(node_pair.second);
            }
        }
        while (!toBeDeleted.empty()) {
            _remove_crossing(*(toBeDeleted.front().lock()));
            toBeDeleted.pop_front();
        }
        _remove_dead_streets();
        _remove_dead_restrictions();
    }

    void OSGraph::only_keep_largest_component(std::list<std::list<std::weak_ptr<OSNode>>> &components) {
        auto largest = components.begin();
        for (auto current = components.begin(); current != components.end(); ++current) {
            if (largest->size() < current->size()) largest = current;
        }
        components.erase(largest);
        for (auto &component : components) {
            for (auto &node : component) {
                _remove_crossing(*node.lock());
            }
        }
        _remove_dead_streets();
        _remove_dead_restrictions();
    }

    void OSGraph::_handle_restrictions(rawNetwork::RawOsmNetwork &raw_network,
                                       way_streets_map_t<std::shared_ptr<OSStreet>> restricted_way_streets) {
        auto restrictions(raw_network.restrictions());
        std::cout << "Handling " << restrictions.size() << " restrictions..." << std::endl;

        int successfullyHandled = 0;
        for (auto &restriction : restrictions) {
            if (_handle_restriction(restriction.second, restricted_way_streets, raw_network)) {
                successfullyHandled += 1;
            } else {
                if (_logRestrictionProcess)
                    std::cout << "Did not handle restriction " << restriction.first << std::endl;
            }
        }
        std::cout << "Handled " << successfullyHandled << " restrictions successfully." << std::endl;
        std::cout << "Registering restriction elements as essential for graph..." << std::endl;
        _num_onlyRestrictions = 0;
        _num_noRestrictions = 0;
        for (auto &res : _onlyRestrictions) {
            _num_onlyRestrictions += 1;
            int count = 0;
            for (auto &eltid : res) {
                if (count % 2 == 0) _osnodes.find(eltid)->second->setToRestriction(true);
                else _osstreets.find(eltid)->second->setToRestriction(true);
                count += 1;
            }
        }
        for (auto &res : _noRestrictions) {
            _num_noRestrictions += 1;
            int count = 0;
            for (auto &eltid : res) {
                if (count % 2 == 0) _osnodes.find(eltid)->second->setToRestriction(true);
                else _osstreets.find(eltid)->second->setToRestriction(true);
                count += 1;
            }
        }
    }

    bool OSGraph::_isFeasibleOnlyRestriction(rawNetwork::RawRestriction &restriction) {
        if (restriction.from_ids().size() > 1 || restriction.to_ids().size() > 1) {
            if (_logRestrictionProcess) {
                std::cout << "Restriction " << restriction.id() << " has more than one from or to way --> skipped"
                          << std::endl;
            }
            return false;

        }
        return true;
    }

    bool OSGraph::_isFeasibleNoRestriction(rawNetwork::RawRestriction &restriction) {
        if (restriction.from_ids().size() > 1 || restriction.to_ids().size() > 1) {
            if (_logRestrictionProcess) {
                std::cout << "Restriction " << restriction.id() << " has more than one from or to way --> Skipped"
                          << std::endl;
            }
            return false;
        }
        return true;
    }

    bool OSGraph::_isFeasibleNoExitRestriction(rawNetwork::RawRestriction &restriction) {
        if (restriction.from_ids().size() > 1) {
            if (_logRestrictionProcess) {
                std::cout << "No Exit Restriction " << restriction.id() << " has more than one from way --> Skipped"
                          << std::endl;
            }
            return false;
        }
        if (restriction.osmelement_via() != NODE) {
            if (_logRestrictionProcess) {
                std::cout << "No Exit Restrictoin " << restriction.id() << " has non-node via member --> Skipped"
                          << std::endl;
            }
            return false;
        }
        return true;
    }

    bool OSGraph::_isFeasibleNoEntryRestriction(rawNetwork::RawRestriction &restriction) {
        if (restriction.to_ids().size() > 1) {
            if (_logRestrictionProcess) {
                std::cout << "No Entry Restriction " << restriction.id() << " has more than one to way --> Skipped"
                          << std::endl;
            }
            return false;
        }
        if (restriction.osmelement_via() != NODE) {
            if (_logRestrictionProcess) {
                std::cout << "No Entry Restrictoin " << restriction.id() << " has non-node via member --> Skipped"
                          << std::endl;
            }
            return false;
        }
        return true;
    }

    bool OSGraph::_isFeasibleRestriction(rawNetwork::RawRestriction &restriction) {
        if (restriction.osmelement_to() != WAY || restriction.osmelement_from() != WAY) {
            if (_logRestrictionProcess) {
                std::cout << "Restriction " << restriction.id()
                          << " has to or from members, which are not ways! --> Skipped" << std::endl;
            }
            return false;
        }
        if (restriction.osmelement_via() != WAY && restriction.osmelement_via() != NODE) {
            if (_logRestrictionProcess) {
                std::cout << "Restriction " << restriction.id()
                          << " has via member which is neither a way nor a node! --> Skipped" << std::endl;
            }
            return false;
        }
        return true;
    }

    bool OSGraph::_isFeasibleRestrictionWithViaNode(rawNetwork::RawRestriction &restriction,
                                                    way_streets_map_t<std::shared_ptr<OSStreet>> &wayToStreets) {
        if (restriction.via_ids().size() > 1) {
            if (_logRestrictionProcess) {
                std::cout << "In restriction " << restriction.id() << ": multiple via node members. ---> skipped!"
                          << std::endl;
            }
            return false;
        }
        for (const auto &from_id : restriction.from_ids()) {
            if (wayToStreets[from_id].front()->start().id() != restriction.via_ids().front()) {
                if (wayToStreets[from_id].back()->end().id() != restriction.via_ids().front()) {
                    if (_logRestrictionProcess) {
                        std::cout << "In restriction " << restriction.id()
                                  << ": from way crosses via node or does not touch it at all. --> skipped!"
                                  << std::endl;
                    }
                    return false;
                }
            } else if (wayToStreets[from_id].front()->oneway()) {
                if (_logRestrictionProcess) {
                    std::cout << "In restriction " << restriction.id()
                              << ": from member of restriction is oneway in wrong direction (automatically included) --> skipped!"
                              << std::endl;
                }
                return false;
            }
        }
        for (const auto &to_id : restriction.to_ids()) {
            if (wayToStreets[to_id].back()->end().id() != restriction.via_ids().front()) {
                if (wayToStreets[to_id].front()->start().id() != restriction.via_ids().front()) {
                    if (_logRestrictionProcess) {
                        std::cout << "In restriction " << restriction.id()
                                  << ": to way crosses via node or does not touch it at all. --> skipped!"
                                  << std::endl;
                    }
                    return false;
                }
            } else if (wayToStreets[to_id].back()->oneway()) {
                if (_logRestrictionProcess) {
                    std::cout << "In restriction " << restriction.id()
                              << ": to member of restriction is oneway in wrong direction (automatically included) --> skipped!"
                              << std::endl;
                }
                return false;
            }
        }

        return true;
    }

    bool OSGraph::_handle_restriction(rawNetwork::RawRestriction &restriction,
                                      way_streets_map_t<std::shared_ptr<OSStreet>> &wayToStreets,
                                      rawNetwork::RawOsmNetwork &raw_network) {
        bool success = false;
        if (_isFeasibleRestriction(restriction)) {
            switch (restriction.type()) {
                case ONLY_LEFT:
                case ONLY_RIGHT:
                case ONLY_STRAIGHT:
                case ONLY_UTURN:
                    success = _handle_only_restriction(restriction, wayToStreets, raw_network);
                    break;
                case NO_LEFT:
                case NO_RIGHT:
                case NO_STRAIGHT:
                case NO_UTURN:
                    success = _handle_no_restriction(restriction, wayToStreets, raw_network);
                    break;
                case NO_EXIT:
                    success = _handle_no_exit_restriction(restriction, wayToStreets, raw_network);
                    break;
                case NO_ENTRY:
                    success = _handle_no_entry_restriction(restriction, wayToStreets, raw_network);
                    break;
                default:
                    if (_logRestrictionProcess) {
                        std::cout << "In restriction " << restriction.id()
                                  << ": Restriction has unknown type! ---> skipped!" << std::endl;
                    }
                    break;
            }
        }
        return success;
    }

    bool OSGraph::_handle_only_restriction(rawNetwork::RawRestriction &restriction,
                                           way_streets_map_t<std::shared_ptr<OSStreet>> &wayToStreets,
                                           rawNetwork::RawOsmNetwork &raw_network) {
        if (!_isFeasibleOnlyRestriction(restriction)) return false;
        std::list<id_type> restrictionList{};
        if (_getOsgElementsInvolvedInRestriction(restriction, wayToStreets, restrictionList)) {
            _onlyRestrictions.push_back(std::move(restrictionList));
            restrictionList.clear();
            return true;
        }
        return false;
    }

    bool OSGraph::_handle_no_restriction(rawNetwork::RawRestriction &restriction,
                                         way_streets_map_t<std::shared_ptr<OSStreet>> &wayToStreets,
                                         rawNetwork::RawOsmNetwork &raw_network) {
        if (!_isFeasibleNoRestriction(restriction)) return false;
        std::list<id_type> restrictionList{};
        if (_getOsgElementsInvolvedInRestriction(restriction, wayToStreets, restrictionList)) {
            _noRestrictions.push_back(std::move(restrictionList));
            restrictionList.clear();
            return true;
        }
        return false;
    }

    void
    OSGraph::_addStreetNodeStreetRestrictionToList(std::list<id_type> &elements, std::shared_ptr<OSStreet> &inStreet,
                                                   id_type crossingid, std::shared_ptr<OSStreet> &outStreet) {
        elements.push_back(inStreet->start().id() == crossingid ? inStreet->end().id() : inStreet->start().id());
        elements.push_back(inStreet->id());
        elements.push_back(crossingid);
        elements.push_back(outStreet->id());
        elements.push_back(outStreet->start().id() == crossingid ? outStreet->end().id() : outStreet->start().id());
    }

    bool OSGraph::_handle_no_entry_restriction(rawNetwork::RawRestriction &restriction,
                                               way_streets_map_t<std::shared_ptr<OSStreet>> &wayToStreets,
                                               rawNetwork::RawOsmNetwork &network) {
        if (!_isFeasibleNoEntryRestriction(restriction) ||
            _isFeasibleRestrictionWithViaNode(restriction, wayToStreets)) {
            return false;
        }
        std::list<std::shared_ptr<OSStreet>> outStreet = _getIncidentStreetsFromList(
                wayToStreets[restriction.to_ids().front()],
                restriction.via_ids().front());
        if (outStreet.size() != 1) return false;
        std::list<id_type> restrictionList{};
        bool addedRestriction{false};
        for (const auto &wayid : restriction.from_ids()) {
            std::list<std::shared_ptr<OSStreet>> inStreet = _getIncidentStreetsFromList(wayToStreets[wayid],
                                                                                        restriction.via_ids().front());
            if (inStreet.size() != 1) continue;
            _addStreetNodeStreetRestrictionToList(restrictionList, inStreet.front(), restriction.via_ids().front(),
                                                  outStreet.front());
            _noRestrictions.push_back(std::move(restrictionList));
            restrictionList.clear();
            addedRestriction = true;
        }
        return addedRestriction;
    }

    bool OSGraph::_handle_no_exit_restriction(rawNetwork::RawRestriction &restriction,
                                              way_streets_map_t<std::shared_ptr<OSStreet>> &wayToStreets,
                                              rawNetwork::RawOsmNetwork &network) {
        if (!_isFeasibleNoExitRestriction(restriction) ||
            !_isFeasibleRestrictionWithViaNode(restriction, wayToStreets)) {
            return false;
        }
        std::list<std::shared_ptr<OSStreet>> inStreet = _getIncidentStreetsFromList(
                wayToStreets[restriction.from_ids().front()],
                restriction.via_ids().front()
        );
        if (inStreet.size() != 1) return false;
        std::list<id_type> restrictionList{};
        bool addedRestriction{false};
        for (const auto &wayid : restriction.to_ids()) {
            std::list<std::shared_ptr<OSStreet>> outStreet = _getIncidentStreetsFromList(wayToStreets[wayid],
                                                                                         restriction.via_ids().front());
            if (outStreet.size() != 1) continue;
            _addStreetNodeStreetRestrictionToList(restrictionList, inStreet.front(), restriction.via_ids().front(),
                                                  outStreet.front());
            _noRestrictions.push_back(std::move(restrictionList));
            restrictionList.clear();
            addedRestriction = true;
        }
        return addedRestriction;
    }

    std::shared_ptr<OSNode> &OSGraph::add_osnode(rawNetwork::RawNode &rawNode) {
        if (_osnodes.find(rawNode.id()) == _osnodes.end()) {
            _osnodes[rawNode.id()] = std::make_shared<OSNode>(
                    rawNode.id(), rawNode.coord().first, rawNode.coord().second,
                    std::move(rawNode.tags()), rawNode.isDeadend()
            );
        }
        return _osnodes.at(rawNode.id());
    }

    void OSGraph::_registerStreetInEndnodes(std::shared_ptr<OSStreet> &street_ptr) {
        street_ptr->start().add_outgoing_street(street_ptr);
        street_ptr->end().add_incoming_street(street_ptr);
        if (!street_ptr->oneway()) {
            street_ptr->start().add_incoming_street(street_ptr);
            street_ptr->end().add_outgoing_street(street_ptr);
        }
    }


    std::shared_ptr<OSStreet>
    OSGraph::add_osstreet(std::unordered_set<id_type> origins, OSNode &start, OSNode &end, waypoints_t &&waypoints,
                          tag_map_t attr, bool oneway, length_t length_meter) {
        id_type id = _nextStreetId++;
        std::shared_ptr<OSStreet> street = std::make_shared<OSStreet>(id, std::move(origins), start, end,
                                                                      std::move(attr), std::move(waypoints), oneway,
                                                                      length_meter);
        _osstreets[id] = street;
        _registerStreetInEndnodes(street);
        return street;
    }

    std::shared_ptr<OSStreet>
    OSGraph::_createNewStreet(std::unordered_set<id_type> origins, waypoints_t &&waypoints, rawNetwork::RawNode &start,
                              rawNetwork::RawNode &end,
                              tag_map_t tags, bool oneway, length_t length_meter) {
        return add_osstreet(std::move(origins), *add_osnode(start), *add_osnode(end),
                            std::move(waypoints), std::move(tags), oneway, length_meter);
    }

    std::shared_ptr<OSStreet>
    OSGraph::_createNewStreet(std::unordered_set<id_type> origins, waypoints_t &&waypoints, OSNode &start, OSNode &end,
                              tag_map_t tags, bool oneway, length_t length_meter) {
        return add_osstreet(std::move(origins), start, end,
                            std::move(waypoints), std::move(tags), oneway, length_meter);
    }

    std::list<std::shared_ptr<OSStreet>>
    OSGraph::_splitOsmWayIntoStreetparts(rawNetwork::RawWay &way, rawNetwork::RawOsmNetwork &network) {
        std::list<std::shared_ptr<OSStreet>> corresponding_streets;
        id_type currentStart = way.waypoints().front();
        waypoints_t currentWayPoints{};
        length_t length{0.0};
        rawNetwork::RawNode *lastWaypoint{nullptr};
        for (id_type nodeid : way.waypoints()) {
            rawNetwork::RawNode &node = network.getNode(nodeid);
            currentWayPoints.push_back({node.lat(), node.lon()});
            if (lastWaypoint) {
                length += tools::computeDistanceBetweenGPSCoordinatesInMeter(lastWaypoint->lon(), lastWaypoint->lat(),
                                                                             node.lon(), node.lat());
                if (_isEssential(node, way)) {
                    auto new_corresponding_street = _createNewStreet(std::unordered_set<id_type>({way.id()}),
                                                                     std::move(currentWayPoints),
                                                                     network.getNode(currentStart), node,
                                                                     way.tags(), way.oneway(), length);
                    currentWayPoints.clear();
                    currentWayPoints.push_back({node.lat(), node.lon()});
                    currentStart = nodeid;
                    corresponding_streets.emplace_back(new_corresponding_street);
                    length = 0;
                }
            }
            lastWaypoint = &node;
        }
        return corresponding_streets;
    }

    bool OSGraph::_isEssential(rawNetwork::RawNode &node, rawNetwork::RawWay &way) {
        for (const auto & kv_pair : _kvNodePairs) {
            if (node.containsTag(kv_pair.first)) {
                for (const auto & value : kv_pair.second) {
                    if (value == node.getTag(kv_pair.first)) return true;
                }
            }
        }
        return node.incidentStreets() != 1 || node.isDeadend();
    }


    std::list<std::shared_ptr<OSStreet>>
    OSGraph::_getIncidentStreetsFromList(std::list<std::shared_ptr<OSStreet>> &streets, id_type nodeid) {
        std::list<std::shared_ptr<OSStreet>> incidentStreets{};
        for (auto &street : streets) {
            if (street->start().id() == nodeid || street->end().id() == nodeid) {
                incidentStreets.push_back(street);
            }
        }
        return std::move(incidentStreets);
    }

    void OSGraph::_remove_crossing(OSNode &crossing) {
        _osnodes.erase(crossing.id());
        _mark_incident_streets_dead(crossing);
    }

    void OSGraph::_remove_crossing(id_type crossingid) {
        auto crossing_it = _osnodes.find(crossingid);
        _osnodes.erase(crossing_it);
        _mark_incident_streets_dead(*(crossing_it->second));
    }

    void OSGraph::_mark_incident_streets_dead(OSNode &node) {
        for (auto &street : node.incoming_streets()) {
            street.lock()->setDangling();
            _deadStreetIds.push_front(street.lock()->id());
        }
        for (auto &street : node.outgoing_streets()) {
            street.lock()->setDangling();
            _deadStreetIds.push_front(street.lock()->id());
        }
    }

    bool OSGraph::_all_elements_of_restriction_in_graph(std::list<id_type> &res) {
        for (auto nodeid = res.begin(); nodeid != res.end(); ++nodeid) {
            if (_osnodes.find(*nodeid) == _osnodes.end()) {
                return false;
            }
            ++nodeid;
            if (nodeid == res.end()) break;
        }
        return true;
    }

    void OSGraph::_remove_dead_restrictions() {
        auto res = _onlyRestrictions.begin();
        bool remove = false;
        while (res != _onlyRestrictions.end()) {
            remove = !_all_elements_of_restriction_in_graph(*res);
            if (remove) res = _onlyRestrictions.erase(res);
            else ++res;
        }
        res = _noRestrictions.begin();
        while (res != _noRestrictions.end()) {
            remove = !_all_elements_of_restriction_in_graph(*res);
            if (remove) res = _noRestrictions.erase(res);
            else ++res;
        }
    }

    void OSGraph::_remove_dead_streets() {
        for (auto &node_ptr : _osnodes) {
            auto street_iter = node_ptr.second->outgoing_streets().begin();
            while (street_iter != node_ptr.second->outgoing_streets().end()) {
                if (street_iter->lock()->dangling())
                    street_iter = node_ptr.second->outgoing_streets().erase(street_iter);
                else ++street_iter;
            }
            street_iter = node_ptr.second->incoming_streets().begin();
            while (street_iter != node_ptr.second->incoming_streets().end()) {
                if (street_iter->lock()->dangling())
                    street_iter = node_ptr.second->incoming_streets().erase(street_iter);
                else ++street_iter;
            }
        }
        while (!_deadStreetIds.empty()) {
            _osstreets.erase(_deadStreetIds.front());
            _deadStreetIds.pop_front();
        }
    }

    bool OSGraph::_getOsgElementsInvolvedInRestrictionViaIsNode(rawNetwork::RawRestriction &restriction,
                                                                way_streets_map_t<std::shared_ptr<OSStreet>> &wayToStreets,
                                                                std::list<id_type> &involvedOsgElements) {
        std::list<std::shared_ptr<OSStreet>> inStreet = _getIncidentStreetsFromList(
                wayToStreets[restriction.from_ids().front()],
                restriction.via_ids().front());
        std::list<std::shared_ptr<OSStreet>> outStreet = _getIncidentStreetsFromList(
                wayToStreets[restriction.to_ids().front()],
                restriction.via_ids().front());
        if (inStreet.size() != 1 || outStreet.size() != 1) {
            if (_logRestrictionProcess) {
                std::cout << "In Streets: ";
                for (auto &street_ptr : inStreet) {
                    std::cout << "start: " << street_ptr->start().id() << " end: " << street_ptr->end().id() << " ";
                }
                std::cout << std::endl << "Out Streets: ";
                for (auto &street_ptr : outStreet) {
                    std::cout << "start: " << street_ptr->start().id() << " end: " << street_ptr->end().id() << " ";
                }
                std::cout << std::endl;
            }
            return false;
        }
        involvedOsgElements.push_back(
                inStreet.front()->start().id() == restriction.via_ids().front() ? inStreet.front()->end().id()
                                                                                : inStreet.front()->start().id());
        involvedOsgElements.push_back(inStreet.front()->id());
        involvedOsgElements.push_back(restriction.via_ids().front());
        involvedOsgElements.push_back(outStreet.front()->id());
        involvedOsgElements.push_back(
                outStreet.front()->start().id() == restriction.via_ids().front() ? outStreet.front()->end().id()
                                                                                 : outStreet.front()->start().id());
        return true;
    }

    void OSGraph::_pushBackStreetAndNodeIds(std::list<id_type> &elements, std::list<std::shared_ptr<OSStreet>> &streets,
                                            id_type endnodeid) {
        id_type startnodeid = elements.back();
        if (endnodeid == startnodeid) return;
        int count = 0;
        int startnodeindex{-1}, endnodeindex{-1};
        bool startInMiddle{false};
        for (auto &street : streets) {
            if (street->start().id() == startnodeid || street->end().id() == startnodeid) {
                if (startnodeindex != -1) startInMiddle = true;
                startnodeindex = count;
            }
            if (street->start().id() == endnodeid || street->end().id() == endnodeid) endnodeindex = count;
            count += 1;
        }
        bool started{false};
        if (startnodeindex <= endnodeindex) {
            int currentIndex{0};
            for (auto &street : streets) {
                if (currentIndex == startnodeindex) started = true;
                if (started) {
                    id_type lastNodeid = elements.back();
                    elements.push_back(street->id());
                    elements.push_back(
                            lastNodeid == street->start().id() ? street->end().id() : street->start().id());
                }
                if (street->start().id() == endnodeid || street->end().id() == endnodeid) return;
                currentIndex += 1;
            }
        } else {
            int currentIndex{0};
            for (auto street_it = streets.rbegin(); street_it != streets.rend(); ++street_it) {
                if ((currentIndex == streets.size() - startnodeindex && startInMiddle) ||
                    (currentIndex == streets.size() - startnodeindex + 1 && !startInMiddle))
                    started = true;
                if (started) {
                    id_type lastNodeid = elements.back();
                    elements.push_back((*street_it)->id());
                    elements.push_back(lastNodeid == (*street_it)->start().id() ? (*street_it)->end().id()
                                                                                : (*street_it)->start().id());
                }
                if ((*street_it)->start().id() == endnodeid || (*street_it)->end().id() == endnodeid) return;
            }
        }

    }

    bool OSGraph::_getOsgElementsInvolvedInRestrictionViaIsWay(rawNetwork::RawRestriction &restriction,
                                                               way_streets_map_t<std::shared_ptr<OSStreet>> &wayToStreets,
                                                               std::list<id_type> &involvedOsgElements) {
        bool entranceFound{false}, exitFound{false};
        std::list<std::shared_ptr<OSStreet>> currentStreets{};

        OSNode *entranceNode{nullptr};
        OSNode *exitNode{nullptr};
        OSNode *currentNode{nullptr};
        id_type lastViaId = 0;
        for (const auto &wayid : restriction.via_ids()) {
            if (lastViaId != 0 && (entranceFound ^ exitFound)) {
                currentNode = _getIntersectionNode(wayToStreets[wayid], wayToStreets[lastViaId]);
                if (currentNode == nullptr) return false;
                _pushBackStreetAndNodeIds(involvedOsgElements, wayToStreets[lastViaId], currentNode->id());
            }
            if (!entranceFound) {
                entranceNode = _getIntersectionNode(wayToStreets[wayid], wayToStreets[restriction.from_ids().front()]);
                if (entranceNode != nullptr) {
                    entranceFound = true;
                    currentStreets = _getIncidentStreetsFromList(wayToStreets[restriction.from_ids().front()],
                                                                 entranceNode->id());
                    if (currentStreets.size() != 1) return false;
                    if (exitFound) {
                        _pushBackStreetAndNodeIds(involvedOsgElements, wayToStreets[wayid], entranceNode->id());

                        involvedOsgElements.push_back(currentStreets.front()->id());
                        involvedOsgElements.push_back(currentStreets.front()->start().id() == entranceNode->id()
                                                      ? currentStreets.front()->end().id()
                                                      : currentStreets.front()->start().id());
                        involvedOsgElements.reverse();
                        return true;
                    } else {
                        involvedOsgElements.push_back(currentStreets.front()->start().id() == entranceNode->id()
                                                      ? currentStreets.front()->end().id()
                                                      : currentStreets.front()->start().id());
                        involvedOsgElements.push_back(currentStreets.front()->id());
                        involvedOsgElements.push_back(entranceNode->id());
                    }
                }
            }
            if (!exitFound) {
                exitNode = _getIntersectionNode(wayToStreets[wayid], wayToStreets[restriction.to_ids().front()]);
                if (exitNode != nullptr) {
                    exitFound = true;
                    currentStreets = _getIncidentStreetsFromList(wayToStreets[restriction.to_ids().front()],
                                                                 exitNode->id());
                    if (currentStreets.size() != 1) return false;
                    if (entranceFound) {
                        _pushBackStreetAndNodeIds(involvedOsgElements, wayToStreets[wayid], exitNode->id());
                        involvedOsgElements.push_back(currentStreets.front()->id());
                        involvedOsgElements.push_back(currentStreets.front()->start().id() == exitNode->id()
                                                      ? currentStreets.front()->end().id()
                                                      : currentStreets.front()->start().id());
                        return true;
                    } else {
                        involvedOsgElements.push_back(currentStreets.front()->start().id() == exitNode->id()
                                                      ? currentStreets.front()->end().id()
                                                      : currentStreets.front()->start().id());
                        involvedOsgElements.push_back(currentStreets.front()->id());
                        involvedOsgElements.push_back(exitNode->id());
                    }
                }
            }
            lastViaId = wayid;
        }
        return false;
    }

    bool OSGraph::_getOsgElementsInvolvedInRestriction(rawNetwork::RawRestriction &restriction,
                                                       way_streets_map_t<std::shared_ptr<OSStreet>> &wayToStreets,
                                                       std::list<id_type> &involvedOsgElements) {
        bool success{false};
        if (restriction.osmelement_via() == NODE) {
            success = _getOsgElementsInvolvedInRestrictionViaIsNode(restriction, wayToStreets, involvedOsgElements);
            if (_logRestrictionProcess && !success) {
                std::cout << "In restriction " << restriction.id()
                          << ": could not identify involved members (via is node)"
                          << std::endl;
            }
        } else {
            success = _getOsgElementsInvolvedInRestrictionViaIsWay(restriction, wayToStreets, involvedOsgElements);
            if (_logRestrictionProcess && !success) {
                std::cout << "In restriction " << restriction.id()
                          << ": could not identify involved members (via is way)"
                          << std::endl;
            }
        }
        return success;
    }

    OSNode *OSGraph::_getIntersectionNode(std::list<std::shared_ptr<OSStreet>> &streets_1,
                                          std::list<std::shared_ptr<OSStreet>> &streets_2) {
        for (const auto &street_1 : streets_1) {
            if (street_1->start() == streets_2.front()->start() || street_1->start() == streets_2.back()->end()) {
                return &street_1->start();
            }
            if (street_1->end() == streets_2.front()->start() || street_1->end() == streets_2.back()->end()) {
                return &street_1->end();
            }
        }
        return nullptr;
    }


    void OSGraph::_fillInCrossing(Json::Value &nodes, OSNode &node, const std::string &nodeid,
                                  const std::unordered_set<std::string> &crossingKeys) {
        Json::Value json_node;
//        for (auto &tagmap : node.tagMap()) {
//            json_node[tagmap.first] = tagmap.second;
//        }
        Json::Value coord(Json::arrayValue);
        coord.append(node.lat());
        coord.append(node.lon());
        json_node[CROSSING_COORD] = coord;
        json_node[CROSSING_ID] = nodeid;
        for (const auto & kv_pair : node.tagMap()) {
            if (crossingKeys.find(kv_pair.first) != crossingKeys.end()) {
                json_node[kv_pair.first] = kv_pair.second;
            }
        }
        nodes.append(json_node);

    }

    void OSGraph::_fillInStreet(Json::Value &streets, OSStreet &street, const std::string &streetId,
                                const std::unordered_set<std::string> &streetKeys) {
        Json::Value json_street;
//        for (auto &tagmap : street.tagMap()) {
//            json_street[tagmap.first] = tagmap.second;
//        }
        Json::Value coords(Json::arrayValue);
        for (const auto &waypoint : street.waypoints()) {
            Json::Value coord(Json::arrayValue);
            coord.append(waypoint[0]);
            coord.append(waypoint[1]);
            coords.append(coord);
        }
        json_street[STREET_ID] = streetId;
        json_street[STREET_COORDS] = coords;
        Json::Value origin(Json::arrayValue);
        for (auto &elt : street.origin()) {
            origin.append(std::to_string(elt));
        }
        json_street[STREET_ORIGIN] = origin;
        json_street[STREET_LENGTH_METER] = street.length_meter();
        json_street[STREET_TYPE] = street.type();
        json_street[STREET_IS_ONEWAY] = street.oneway();
        json_street[STREET_NUM_LANES] = street.lanes();
        json_street[STREET_MAXSPEED] = street.maxspeed();
        json_street[STREET_START] = std::to_string(street.start().id());
        json_street[STREET_END] = std::to_string(street.end().id());
        for (const auto & kv_pair : street.tagMap()) {
            if (streetKeys.find(kv_pair.first) != streetKeys.end()) {
                json_street[kv_pair.first] = kv_pair.second;
            }
        }
        streets.append(json_street);
    }

    void OSGraph::_fillInStreetsAndCrossings(Json::Value &root, const std::unordered_set<std::string> &streetKeys,
                                             const std::unordered_set<std::string> &crossingKeys) {
        Json::Value streets(Json::arrayValue);
        Json::Value nodes(Json::arrayValue);
        std::unordered_map<id_type, int> addedNodes{};
        std::string currentStreetId{};
        for (auto &street : _osstreets) {
            currentStreetId = std::to_string(street.second->id());
            _fillInStreet(streets, *(street.second), currentStreetId, streetKeys);
            if (addedNodes.find(street.second->start().id()) == addedNodes.end()) {
                _fillInCrossing(nodes, street.second->start(), std::to_string(street.second->start().id()),
                                crossingKeys);
                addedNodes[street.second->start().id()] = 0;
            }
            if (addedNodes.find(street.second->end().id()) == addedNodes.end()) {
                _fillInCrossing(nodes, street.second->end(), std::to_string(street.second->end().id()),
                                crossingKeys);
                addedNodes[street.second->end().id()] = 0;
            }
        }
        std::unordered_map<id_type, int> incomingStreets{};
        std::unordered_map<id_type, int> outgoingStreets{};
        for (const auto &node : _osnodes) {
            if (addedNodes.find(node.second->id()) == addedNodes.end()) {
                std::cout << "Node " << node.second->id() << " does not have any according streets." << std::endl;
                for (const auto &street : node.second->incoming_streets()) {
                    std::cout << "Incoming street is " << street.lock()->id() << " and is oneway "
                              << street.lock()->oneway() << std::endl;
                }
                for (const auto &street : node.second->outgoing_streets()) {
                    std::cout << "Outgoing street is " << street.lock()->id() << " and is oneway "
                              << street.lock()->oneway() << std::endl;
                }
            }
            for (const auto &street : node.second->incoming_streets()) {
                if (incomingStreets.find(street.lock()->id()) != incomingStreets.end()) {
                    if (street.lock()->oneway() || incomingStreets[street.lock()->id()] >= 2) {
                        std::cout << "Street " << street.lock()->id() << " is registered multiple times as incoming!"
                                  << std::endl;
                        std::cout << "Is oneway: " << street.lock()->oneway() << std::endl;
                        std::cout << "Current node: " << node.second->id() << std::endl;
                        std::cout << "Street start: " << street.lock()->start().id() << " Street end: "
                                  << street.lock()->end().id() << std::endl;
                    }
                    incomingStreets[street.lock()->id()] += 1;
                } else {
                    incomingStreets.insert({street.lock()->id(), 1});
                }

                if (_osstreets.find(street.lock()->id()) == _osstreets.end()) {
                    std::cout << "Node " << node.second->id() << " has incident street that does not exist."
                              << std::endl;
                }
            }
            for (const auto &street:node.second->outgoing_streets()) {
                if (outgoingStreets.find(street.lock()->id()) != outgoingStreets.end()) {
                    if (street.lock()->oneway() || outgoingStreets[street.lock()->id()] >= 2) {
                        std::cout << "Street " << street.lock()->id() << " is registered multiple times as outgoing!"
                                  << std::endl;
                        std::cout << "Is oneway: " << street.lock()->oneway() << std::endl;
                        std::cout << "Current node: " << node.second->id() << std::endl;
                        std::cout << "Street start: " << street.lock()->start().id() << " Street end: "
                                  << street.lock()->end().id() << std::endl;
                    }
                    outgoingStreets[street.lock()->id()] += 1;

                } else {
                    outgoingStreets.insert({street.lock()->id(), 1});
                }

                if (_osstreets.find(street.lock()->id()) == _osstreets.end()) {
                    std::cout << "Node " << node.second->id() << " has incident street that does not exist."
                              << std::endl;
                }
            }
        }
        if (incomingStreets.size() != _osstreets.size()) {
            std::cout << "Number of incoming streets (" << incomingStreets.size()
                      << ") does not match overall number of streets (" << _osstreets.size() << ")" << std::endl;
        }
        if (outgoingStreets.size() != _osstreets.size()) {
            std::cout << "Number of outgoing streets (" << outgoingStreets.size()
                      << ") does not match overall number of streets (" << _osstreets.size() << ")" << std::endl;
        }
        root[STREETS] = streets;
        root[CROSSINGS] = nodes;
    }

    int OSGraph::writeGraphToJson(const std::string &filename, const std::unordered_set<std::string> &streetKeys,
                                  const std::unordered_set<std::string> &crossingKeys) {
        Json::Value root;
        std::cout << "Filling in streets and nodes..." << std::endl;
        _fillInStreetsAndCrossings(root, streetKeys, crossingKeys);
        std::cout << "Filling in restrictions..." << std::endl;
        _fillInRestrictions(root);
        Json::Value groups;
        root[GROUPS] = groups;
        return _write(root, filename);
    }

    int OSGraph::_write(Json::Value &root, const std::string &filename) {
        Json::StreamWriterBuilder builder;
        builder.settings_["precision"] = 10;
        builder["commentStyle"] = "None";
        builder["indentation"] = "   ";
        std::unique_ptr<Json::StreamWriter> writer(builder.newStreamWriter());
        std::ofstream outputFileStream(filename);
        writer->write(root, &outputFileStream);
        outputFileStream.close();
        std::cout << "Wrote OpenStreetGraph to file " << filename << std::endl;
        return 1;
    }

    void OSGraph::_appendNode(std::shared_ptr<OSNode> &node, Json::Value &features,
                              const std::unordered_set<std::string> &crossingKeys) {
        Json::Value crossing;
        crossing[geojson::TYPE] = geojson::FEATURE;
        Json::Value geometry;
        geometry[geojson::TYPE] = geojson::POINT;
        Json::Value coord(Json::arrayValue);
        coord.append(node->lon());
        coord.append(node->lat());
        geometry[geojson::COORDINATES] = coord;
        crossing[geojson::GEOMETRY] = geometry;
        crossing[geojson::ID] = std::to_string(node->id());
        Json::Value properties;
        properties[geojson::NAME] = CROSSING;
        for (const auto & kv_pair : node->tagMap()) {
            if (crossingKeys.find(kv_pair.first) != crossingKeys.end()) {
                properties[kv_pair.first] = kv_pair.second;
            }
        }
        crossing[geojson::PROPERTIES] = properties;
        features.append(crossing);
    }

    void OSGraph::_appendCoord(Json::Value &coord, const std::array<coord_t, 2> &wp, std::array<coord_t, 4> &bbox) {
        coord.append(wp[1]);
        coord.append(wp[0]);
        bbox[0] = std::min(bbox[0], wp[1]);
        bbox[1] = std::min(bbox[1], wp[0]);
        bbox[2] = std::max(bbox[2], wp[1]);
        bbox[3] = std::max(bbox[2], wp[0]);
    }

    void
    OSGraph::_appendStreet(std::shared_ptr<OSStreet> &street, Json::Value &features, std::array<coord_t, 4> &bbox,
                           const std::unordered_set<std::string> &streetKeys) {
        Json::Value json_street;
        json_street[geojson::TYPE] = geojson::FEATURE;
        Json::Value geometry;
        geometry[geojson::TYPE] = geojson::LINE;
        Json::Value coords(Json::arrayValue);
        for (const auto &waypoint : street->waypoints()) {
            Json::Value coord(Json::arrayValue);
            _appendCoord(coord, waypoint, bbox);
            coords.append(coord);
        }
        geometry[geojson::COORDINATES] = coords;
        json_street[geojson::GEOMETRY] = geometry;
        json_street[geojson::ID] = "s_" + std::to_string(street->id());
        Json::Value properties;
        Json::Value origin(Json::arrayValue);
        for (auto &elt : street->origin()) {
            origin.append(std::to_string(elt));
        }
        properties[STREET_ORIGIN] = origin;
        properties[STREET_LENGTH_METER] = street->length_meter();
        properties[STREET_TYPE] = street->type();
        properties[STREET_IS_ONEWAY] = street->oneway();
        //properties[STREET_NUM_LANES] = street->lanes();
        //properties[STREET_MAXSPEED] = street->maxspeed();
        properties[STREET_START] = std::to_string(street->start().id());
        properties[STREET_END] = std::to_string(street->end().id());
        for (const auto & kv_pair : street->tagMap()) {
            if (streetKeys.find(kv_pair.first) != streetKeys.end()) {
                properties[kv_pair.first] = kv_pair.second;
            }
        }
        properties[geojson::NAME] = STREET;
        json_street[geojson::PROPERTIES] = properties;
        features.append(json_street);
    }

    void OSGraph::_appendRestrictions(std::list<id_type> &res, Json::Value &features, const std::string &resType,
                                      int count) {
        Json::Value restriction;
        Json::Value properties;
        properties[geojson::NAME] = RESTRICTION;
        properties[RESTRICTION_TYPE] = resType;
        Json::Value geometry;
        geometry[geojson::TYPE] = geojson::LINE;
        Json::Value coordinates(Json::arrayValue);
        int i = 0;
        Json::Value resElts(Json::arrayValue);
        std::shared_ptr<OSNode> currentNode;
        std::shared_ptr<OSStreet> currentStreet;
        for (auto &elt : res) {
            if (i % 2 == 0) {
                currentNode = _osnodes.find(elt)->second;
                resElts.append(std::to_string(elt));
            } else {
                resElts.append("s_" + std::to_string(elt));
                currentStreet = _osstreets.find(elt)->second;
                if (currentStreet->start() == *currentNode) {
                    for (auto &wp : currentStreet->waypoints()) {
                        Json::Value coord(Json::arrayValue);
                        coord.append(wp[1]);
                        coord.append(wp[0]);
                        coordinates.append(coord);
                    }
                } else if (currentStreet->end() == *currentNode) {
                    for (auto wp = currentStreet->waypoints().rbegin(); wp != currentStreet->waypoints().rend(); ++wp) {
                        Json::Value coord(Json::arrayValue);
                        coord.append((*wp)[1]);
                        coord.append((*wp)[0]);
                        coordinates.append(coord);
                    }
                } else {
                    std::cout << "Restriction with elt " << elt << " has unfitting street" << std::endl;
                }
            }
            ++i;
        }
        properties[RESTRICTION_ELEMENTS] = resElts;
        geometry[geojson::COORDINATES] = coordinates;
        restriction[geojson::GEOMETRY] = geometry;
        restriction[geojson::PROPERTIES] = properties;
        restriction[geojson::TYPE] = geojson::FEATURE;
        restriction[geojson::ID] = "r_" + std::to_string(count);
        features.append(restriction);
    }

    void OSGraph::_addStreetsAndCrossingsAsGeo(Json::Value &features, std::array<coord_t, 4> &bbox,
                                               const std::unordered_set<std::string> &streetKeys,
                                               const std::unordered_set<std::string> &crossingKeys) {
        for (auto &node: _osnodes) {
            _appendNode(node.second, features, crossingKeys);
        }
        for (auto &street: _osstreets) {
            _appendStreet(street.second, features, bbox, streetKeys);
        }
        int restriction_count = 0;
        for (auto &res: _onlyRestrictions) {
            _appendRestrictions(res, features, ONLY_RESTRICTIONS, ++restriction_count);
        }
        for (auto &res: _noRestrictions) {
            _appendRestrictions(res, features, NO_RESTRICTIONS, ++restriction_count);
        }
    }

    int OSGraph::writeGraphToGeoJson(const std::string &filename, const std::unordered_set<std::string> &streetKeys,
                                     const std::unordered_set<std::string> &crossingKeys)

    {
        Json::Value root;
        root[geojson::TYPE] = geojson::FEATURE_COLLECTION;
        Json::Value features(Json::arrayValue);
        std::array<coord_t, 4> array_bbox{std::numeric_limits<coord_t>::max(), std::numeric_limits<coord_t>::max(),
                                          std::numeric_limits<coord_t>::min(), std::numeric_limits<coord_t>::min()};
        _addStreetsAndCrossingsAsGeo(features, array_bbox, streetKeys, crossingKeys);
        Json::Value bbox(Json::arrayValue);
        for (auto &number : array_bbox) {
            bbox.append(number);
        }
        root[geojson::BBOX] = bbox;
        root[geojson::FEATURES] = features;
        return _write(root, filename);
    }

    void OSGraph::_fillInRestrictions(Json::Value &root) {
        Json::Value restrictions;
        Json::Value only_restrictions(Json::arrayValue);
        Json::Value no_restrictions(Json::arrayValue);
        for (auto &res : _onlyRestrictions) {
            Json::Value only_res(Json::arrayValue);
            for (auto &elt : res) {
                only_res.append(std::to_string(elt));
            }
            only_restrictions.append(only_res);
        }
        for (auto &res: _noRestrictions) {
            Json::Value no_res(Json::arrayValue);
            for (auto &elt : res) {
                no_res.append(std::to_string(elt));
            }
            no_restrictions.append(no_res);
        }
        restrictions[ONLY_RESTRICTIONS] = only_restrictions;
        restrictions[NO_RESTRICTIONS] = no_restrictions;
        root[RESTRICTIONS] = restrictions;
    }

    bool OSGraph::_sameTags(OSStreet &street1, OSStreet &street2, std::unordered_set<std::string> &streetKeysForEqual) {
        if (street1.oneway() != street2.oneway()) return false;
        if (street1.type() != street2.type()) return false;
        for (const auto & key : streetKeysForEqual) {
            if (street1.containsTag(key) != street2.containsTag(key)) return false;
            if (street1.containsTag(key) && (street1.getTag(key) != street2.getTag(key))) return false;
        }
        return true;
    }

    void OSGraph::mergeDegreeTwoCrossings(std::unordered_set<std::string> &streetKeysForEqual) {
        std::list<id_type> deletedNodes{};
        bool oneway{false};
        std::shared_ptr<OSStreet> inStreet, outStreet;
        for (auto &node : _osnodes) {
            if (node.second->outgoing_streets().empty() || node.second->incoming_streets().empty() ||
                node.second->isContainedInRestriction() || node.second->hasKVPair(_kvNodePairs))
                continue;
            oneway = node.second->outgoing_streets().front().lock()->oneway();
            if (oneway) {
                if (node.second->outdegree() != 1 || node.second->indegree() != 1) continue;
                inStreet = node.second->incoming_streets().front().lock();
                outStreet = node.second->outgoing_streets().front().lock();
            } else {
                if (node.second->outdegree() != 2 || node.second->indegree() != 2) continue;
                inStreet = node.second->incoming_streets().front().lock();
                outStreet = node.second->incoming_streets().back().lock();
                for (auto &street_ptr : node.second->outgoing_streets()) {
                    if (street_ptr.lock()->id() != inStreet->id() && street_ptr.lock()->id() != outStreet->id())
                        continue;
                }
            }
            if (inStreet->id() == outStreet->id()) continue;
            if (inStreet->isContainedInRestriction() || outStreet->isContainedInRestriction()) continue;
            if (!_sameTags(*inStreet, *outStreet, streetKeysForEqual)) continue;
            auto newstreet = _resolveDegreeTwoNode(*(node.second), *inStreet, *outStreet);
            newstreet->start().incoming_streets().remove_if([inStreet, outStreet](std::weak_ptr<OSStreet> &s) {
                return (s.lock()->id() == inStreet->id()) || (s.lock()->id() == outStreet->id());
            });
            newstreet->start().outgoing_streets().remove_if([inStreet, outStreet](std::weak_ptr<OSStreet> &s) {
                return (s.lock()->id() == inStreet->id()) || (s.lock()->id() == outStreet->id());
            });
            newstreet->end().incoming_streets().remove_if([inStreet, outStreet](std::weak_ptr<OSStreet> &s) {
                return (s.lock()->id() == inStreet->id()) || (s.lock()->id() == outStreet->id());
            });
            newstreet->end().outgoing_streets().remove_if([inStreet, outStreet](std::weak_ptr<OSStreet> &s) {
                return (s.lock()->id() == inStreet->id()) || (s.lock()->id() == outStreet->id());
            });
            _osstreets.erase(inStreet->id());
            _osstreets.erase(outStreet->id());
            deletedNodes.push_back(node.second->id());

        }
        std::cout << "Smoothing out " << deletedNodes.size() << " degree 2 nodes..." << std::endl;
        for (auto nodeid : deletedNodes) {
            _osnodes.erase(nodeid);
        }
    }

    std::shared_ptr<OSStreet> OSGraph::_resolveDegreeTwoNode(OSNode &node, OSStreet &street1, OSStreet &street2) {
        waypoints_t waypoints = street1.waypoints();
        length_t length = street1.length_meter() + street2.length_meter();
        std::unordered_set<id_type> origins{};
        for (auto &elt : street1.origin()) {
            origins.insert(elt);
        }
        for (auto &elt: street2.origin()) {
            origins.insert(elt);
        }
        if (street1.end() == node) {
            if (street2.start() == node) {
                for (auto coord_it = std::next(street2.waypoints().begin());
                     coord_it != street2.waypoints().end(); ++coord_it) {
                    waypoints.push_back(*coord_it);
                }
                return _createNewStreet(origins, std::move(waypoints), street1.start(), street2.end(),
                                        std::move(street1.tagMap()), street1.oneway(), length);
            } else {
                for (auto it = std::next(street2.waypoints().rbegin()); it != street2.waypoints().rend(); ++it) {
                    waypoints.push_back(*it);
                }
                return _createNewStreet(origins, std::move(waypoints), street1.start(), street2.start(),
                                        std::move(street1.tagMap()), street1.oneway(), length);
            }
        } else {
            if (street2.start() == node) {
                for (auto coord_it = std::next(street2.waypoints().begin());
                     coord_it != street2.waypoints().end(); ++coord_it) {
                    waypoints.push_front(*coord_it);
                }
                return _createNewStreet(std::move(origins), std::move(waypoints), street2.end(), street1.end(),
                                        std::move(street1.tagMap()), street1.oneway(), length);
            } else {
                for (auto it = std::next(street2.waypoints().rbegin()); it != street2.waypoints().rend(); ++it) {
                    waypoints.push_front(*it);
                }
                return _createNewStreet(std::move(origins), std::move(waypoints), street2.start(), street1.end(),
                                        std::move(street1.tagMap()), street1.oneway(), length);
            }
        }


    }

    bool OSGraph::containsEdge(OSNode &start, OSNode &end, std::shared_ptr<OSStreet> &street) {
        for (auto & street2 : start.outgoing_streets()) {
            if (*street == *(street2.lock())) return true;
        }
        return false;
    }
}