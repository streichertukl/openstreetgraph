/*
* Licensed under the MIT license:
*
* Copyright (c) 2020 Manuel Streicher

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*
*/

#pragma once

#include <list>
#include <tuple>

#include "../config.h"
#include "../constants.h"
#include "os_element.h"
#include "node.fwd.h"


namespace tuk::osg::osgraph {
    class OSStreet: public OSGElement, public std::enable_shared_from_this<OSStreet> {
    public:
        OSStreet(id_type id, std::unordered_set<id_type> &&origins, tuk::osg::osgraph::OSNode &start,
                 tuk::osg::osgraph::OSNode &end, tag_map_t &&tags, waypoints_t &&waypoints, bool oneway,
                 double length_meter);

        OSG_ELEMENT getName() {return OSG_STREET;};

        double length_meter() {return _length_meter;};
        OSNode& start() {return _start;};
        OSNode& end() {return _end;};
        bool oneway() {return _oneway;};
        std::unordered_set<id_type> &origin() {return _origin;};
        const waypoints_t &waypoints() const {return _waypoints;};
        //waypoints_t waypoints() {return _waypoints;};
        std::string getTag(const std::string& key);
        bool containsTag(const std::string& key) {return _tags.find(key) != _tags.end();};
        tag_map_t & tagMap() {return _tags;};
        std::string type() {return getTag(TYPE);};
        int maxspeed();
        int lanes();
        void addWaypointFront(coord_pair_t coord);
        void addWaypointBack(coord_pair_t coord);
        void setDangling() {_isDangling=true;};
        bool dangling() {return _isDangling;};

    private:
        std::unordered_set<id_type> _origin;
        const double _length_meter;
        OSNode& _start;
        OSNode& _end;
        tag_map_t _tags;
        waypoints_t _waypoints;
        bool _oneway;
        bool _isDangling {false};
    };
}
