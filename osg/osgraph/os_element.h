/*
* Licensed under the MIT license:
*
* Copyright (c) 2020 Manuel Streicher

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*
*/

#pragma once

#include "../config.h"
#include "../constants.h"
#include "os_graph_types.h"
#include <iostream>
#include <memory>

namespace tuk::osg::osgraph {
    class OSGElement {
    public:
        explicit OSGElement(id_type id) : _id(id) {};
        virtual ~OSGElement() {};

        id_type id() { return _id; };

        friend bool operator==(OSGElement &lhs, OSGElement &rhs) {
            return lhs.id() == rhs.id() && lhs.getName() == rhs.getName();
        };

        friend bool operator!=(OSGElement &lhs, OSGElement &rhs) {
            return lhs.id() != rhs.id() || lhs.getName() != rhs.getName();
        };

        virtual OSG_ELEMENT getName() = 0;

        void setToRestriction(bool b) {_isContainedInRestriction = b;};
        bool isContainedInRestriction() {return _isContainedInRestriction;};
    private:
        const id_type _id;

        bool _isContainedInRestriction{false};
    };
}
