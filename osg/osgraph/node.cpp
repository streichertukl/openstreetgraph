/*
* Licensed under the MIT license:
*
* Copyright (c) 2020 Manuel Streicher

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*
*/

#include "node.h"

void tuk::osg::osgraph::OSNode::add_outgoing_street(std::weak_ptr<OSStreet> street) {
    _outgoing_streets.push_front(std::move(street));
}

void tuk::osg::osgraph::OSNode::add_incoming_street(std::weak_ptr<OSStreet> street) {
    _incoming_streets.push_front(std::move(street));
}

bool tuk::osg::osgraph::OSNode::isIncidentStreet(id_type streetid) {
    for (auto &street_ptr : _outgoing_streets) {
        if (street_ptr.lock()->id() == streetid) return true;
    }
    for (auto & street_ptr : _incoming_streets) {
        if (street_ptr.lock()->id() == streetid) return true;
    }
    return false;
}

bool tuk::osg::osgraph::OSNode::hasKVPair(std::unordered_map<std::string, std::list<std::string>> & dict) {
    for (const auto & kv_pair : dict) {
        if (_tags.find(kv_pair.first) != _tags.end()) {
            for (const auto & val : kv_pair.second) {
                if (val == _tags[kv_pair.first]) return true;
            }
        }
    }
    return false;
}
