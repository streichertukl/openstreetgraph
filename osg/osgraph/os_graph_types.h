/*
* Licensed under the MIT license:
*
* Copyright (c) 2020 Manuel Streicher

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*
*/

#pragma once

#include <forward_list>
#include <list>
#include <tuple>
#include <unordered_map>
#include <memory>

#include "../config.h"

namespace tuk::osg::osgraph {
    using tag_maps_t = std::unordered_map<id_type, tag_map_t>;
    using adj_list_t = std::list<id_type>;
    using adj_lists_t = std::unordered_map<id_type, adj_list_t>;
    using edge_tag_map_t = std::unordered_map<id_type, std::unordered_map<id_type, tag_map_t>>;
    using node_coords_t = std::unordered_map<id_type, coord_pair_t>;
    template<class T>
    using way_streets_map_t = std::unordered_map<id_type, std::list<T>>;
    template<class T>
    using way_streets_map_v_t = std::list<T>;
    template<class T>
    using edge_attr_t_v = std::unordered_map<id_type, std::pair<bool, std::shared_ptr<T>>>;
    template<class T>
    using edge_attr_t = std::unordered_map<id_type, edge_attr_t_v<T>>;


}
