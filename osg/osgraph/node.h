/*
* Licensed under the MIT license:
*
* Copyright (c) 2020 Manuel Streicher

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*
*/

# pragma once

#include "../config.h"
#include "os_graph_types.h"
#include "os_element.h"
#include "street.h"

#include <forward_list>
#include <memory>

namespace tuk::osg::osgraph {
    class OSNode : public OSGElement, public std::enable_shared_from_this<OSNode>{
    public:
        OSNode(id_type id, coord_t lon, coord_t lat, tag_map_t &&tags, bool deadend)
                : OSGElement(id), _lon(lon), _lat (lat), _tags (tags), _isDeadend (deadend){}

        OSG_ELEMENT getName() override {return OSG_NODE;};

        void add_outgoing_street(std::weak_ptr<OSStreet> street);
        void add_incoming_street(std::weak_ptr<OSStreet> street);

        std::list<std::weak_ptr<OSStreet>>& incoming_streets() {return _incoming_streets;};
        std::list<std::weak_ptr<OSStreet>>& outgoing_streets() {return _outgoing_streets;};


        bool isIncidentStreet(id_type streetid);

        bool hasKVPair(std::unordered_map<std::string, std::list<std::string>>& dict);

        coord_t lon() {return _lon;};
        coord_t lat() {return _lat;};

        unsigned long degree() {return _incoming_streets.size() + _outgoing_streets.size();};
        unsigned long outdegree() {return _outgoing_streets.size();};
        unsigned long indegree() {return _incoming_streets.size();};

        bool isDeadend() {return _isDeadend;};

        tag_map_t &tagMap() {return _tags;};

    private:
        const coord_t _lon;
        const coord_t _lat;
        tag_map_t _tags;

        std::list<std::weak_ptr<OSStreet>> _outgoing_streets {};
        std::list<std::weak_ptr<OSStreet>> _incoming_streets {};
        bool _isDeadend;
        bool _isEssential {false};
    };

}