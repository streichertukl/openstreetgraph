/*
* Licensed under the MIT license:
*
* Copyright (c) 2020 Manuel Streicher

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*
*/

#include "distances.h"

double
tuk::osg::osgraph::tools::computeDistanceBetweenGPSCoordinatesInMeter(double x_lon, double x_lat, double y_lon,
                                                                      double y_lat) {
    double d_lat = M_PI * (y_lat - x_lat) / 180;
    double d_lon = M_PI * (y_lon - x_lon) / 180;

    x_lon = M_PI * x_lon / 180;
    x_lat = M_PI * x_lat / 180;
    y_lon = M_PI * y_lon / 180;
    y_lat = M_PI * y_lat / 180;

    double radiusEarth = 6371.0 * 1000;

    double a = sin(d_lat / 2) * sin(d_lat / 2) + sin(d_lon /2) * sin(d_lon / 2) * cos(x_lat) * cos(y_lat);

    return radiusEarth * 2 * atan2(sqrt(a), sqrt(1-a));
}
