/*
* Licensed under the MIT license:
*
* Copyright (c) 2020 Manuel Streicher

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*
*/

#pragma once

#include <iostream>
#include <string>
#include <unordered_set>
#include <list>

namespace tuk::osg {
    const std::string TRUE{"yes"};
    const std::string FALSE{"false"};
    const std::string ONEWAY{"oneway"};
    const std::unordered_set<std::string> WRONGWAY_VALUES{{
                                                                  "-1",
                                                                  "reverse",
                                                                  "wrongway"
                                                          }};
    const std::unordered_set<std::string> NO_ONEWAY_VALUES{{
                                                                   "no",
                                                                   "false",
                                                                   "0",
                                                                   "reversible",
                                                                   "alternating"

                                                           }};
    const std::unordered_set<std::string> ONEWAY_VALUES{{
                                                                "yes",
                                                                "true",
                                                                "1"
                                                        }};
    const std::string TYPE{"highway"};
    const std::string LANES{"lanes"};
    const std::string MAXSPEED{"maxspeed"};
    const std::string JUNCTION{"junction"};
    const std::string ROUNDABOUT{"roundabout"};
    const std::string MOTORWAY_STRING{"motorway"};
    const std::string S_SIMPLIFY{"simplify"};
    const std::string S_STRONG_CONNECT{"strongly_connected"};
    const std::string S_STREET_KEYS{"way_keys_to_transfer"};
    const std::string S_BOUNDING_POLYGON{"bounding_polygon_filename"};
    const std::unordered_set<std::string> STREET_KEYS {
        MAXSPEED
    };
    const std::string S_STREET_VALUES_SAME{"way_values_equal_for_merge"};
    const std::unordered_set<std::string> STREET_VALUES_SAME {
        MAXSPEED,
    };
    const std::string S_CROSSING_KEYS{"node_keys_to_transfer"};
    const std::unordered_set<std::string> CROSSING_KEYS {};
    const std::string S_CROSSING_KV_PAIRS_CONSERVE{"node_key_value_pairs_conserve"};
    const std::list<std::array<std::string, 2>> CROSSING_KV_PAIRS_CONSERVE {};
    const std::string S_HIGHWAY_TYPES {"highway_types"};
    const std::unordered_set<std::string> HIGHWAY_TYPES_SERVICE{{
                                                                "living_street",
                                                                "motorway",
                                                                "motorway_link",
                                                                "primary",
                                                                "primary_link",
                                                                "residential",
                                                                "secondary",
                                                                "secondary_link",
                                                                "tertiary",
                                                                "tertiary_link",
                                                                "trunk",
                                                                "trunk_link",
                                                                "unclassified",
                                                                "service"
                                                        }};
    const std::unordered_set<std::string> HIGHWAY_TYPES{{
                                                                "living_street",
                                                                "motorway",
                                                                "motorway_link",
                                                                "primary",
                                                                "primary_link",
                                                                "residential",
                                                                "secondary",
                                                                "secondary_link",
                                                                "tertiary",
                                                                "tertiary_link",
                                                                "trunk",
                                                                "trunk_link",
                                                                "unclassified"
                                                        }};

    enum OSG_ELEMENT {
        OSG_NODE,
        OSG_STREET,
        NO_OSG_ELEMENT
    };

    enum OsmElement {
        NO_ELEMENT,
        NODE,
        WAY,
        RELATION
    };

    OsmElement getElementType(const std::string &typeName);

    enum RestrictionType {
        NO_RESTRICTION,
        ONLY_RIGHT,
        ONLY_LEFT,
        ONLY_STRAIGHT,
        ONLY_UTURN,
        NO_LEFT,
        NO_RIGHT,
        NO_STRAIGHT,
        NO_UTURN,
        NO_ENTRY,
        NO_EXIT,

    };

    enum RestrictionType getRestrictionType(const std::string &restriction);

    enum HighwayType {
        NO_HIGHWAY,
        MOTORWAY,
        TRUNK,
        PRIMARY,
        SECONDARY,
        TERTIARY,
        UNCLASSIFIED,
        RESIDENTIAL,
        MOTORWAY_LINK,
        TRUNK_LINK,
        PRIMARY_LINK,
        SECONDARY_LINK,
        TERTIARY_LINK,
        LIVING_STREET,
        SERVICE,
        PEDESTRIAN,
        TRACK,
        BUS_GUIDEWAY,
        ESCAPE,
        RACEWAY,
        ROAD,
        FOOTWAY,
        BRIDLEWAY,
        STEPS,
        PATH,
        CYCLEWAY,
        CONSTRUCTION
    };

    enum HighwayType getWaytype(const std::string &highway);
}

namespace tuk::osg::osgraph {

    const std::string STREETS = "streets";
    const std::string STREET = "street";
    const std::string CROSSINGS = "crossings";
    const std::string CROSSING = "crossing";
    const std::string GROUPS = "groups";
    const std::string RESTRICTIONS = "restrictions";
    const std::string RESTRICTION = "restriction";

    //Street attribute keys;
    const std::string STREET_ID = "id";
    const std::string STREET_ORIGIN = "origin";
    const std::string STREET_START = "start";
    const std::string STREET_END = "end";
    const std::string STREET_COORDS = "coords";
    const std::string STREET_NUM_LANES = "lanes";
    const std::string STREET_TYPE = "type";
    const std::string STREET_MAXSPEED = "maxspeed";
    const std::string STREET_LENGTH_METER = "length_in_m";
    const std::string STREET_IS_ONEWAY = "oneway";

    const int NO_MAX_SPEED = 0;
    const int LANE_STANDARD = 1;

    //CROSSING attribute keys;
    const std::string CROSSING_ID = "id";
    const std::string CROSSING_COORD = "coord";

    //RESTRICTION KEYS:
    const std::string RESTRICTION_TYPE = "restriction_type";
    const std::string RESTRICTION_ELEMENTS = "restriction_elements";
    const std::string ONLY_RESTRICTIONS = "only";
    const std::string NO_RESTRICTIONS = "no";


}
namespace tuk::osg::osgraph::geojson {
    const std::string TYPE = "type";
    const std::string NAME = "osm_type";
    const std::string ID = "id";

    const std::string BBOX = "bbox";
    const std::string FEATURE = "Feature";
    const std::string FEATURE_COLLECTION = "FeatureCollection";
    const std::string FEATURES = "features";
    const std::string PROPERTIES = "properties";
    const std::string COORDINATES = "coordinates";

    const std::string GEOMETRY = "geometry";
    const std::string GEOMETRIES = "geometries";

    const std::string LINE = "LineString";
    const std::string MULTI_LINE = "MultiLineString";
    const std::string POINT = "Point";
    const std::string MULTI_POINT = "MultiPoint";
    const std::string POLYGON = "Polygon";
    const std::string MULTI_POLYGON = "MultiPolygon";

    const std::string GEOMETRY_COLLECTION = "GeometryCollection";
}
