/*
* Licensed under the MIT license:
*
* Copyright (c) 2020 Manuel Streicher

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*
*/

#pragma once

#include <memory>
#include <string>

#include "../raw_osm_network/raw_osm_network.h"

namespace tuk::osg::osmparser
{
  class OSMParser
  {
    public:
      OSMParser (const std::string & filename, const std::unordered_set<std::string> &highwayTypes,
                 const std::list<std::array<std::string, 2>> &kvNodePairs)
      : _osmfile (filename)
      , _network (std::make_shared<rawNetwork::RawOsmNetwork>())
      , _highwayTypes (highwayTypes)
      , _kvNodePairs (kvNodePairs)
      {}
      ~OSMParser() {}

      virtual void parse() = 0;
      
      std::shared_ptr<rawNetwork::RawOsmNetwork> network()
      {
        return (_network);
      };
    
    protected:
      const std::string _osmfile;
      std::shared_ptr<rawNetwork::RawOsmNetwork> _network;
      const std::unordered_set<std::string> & _highwayTypes;
      const std::list<std::array<std::string, 2>> & _kvNodePairs;
  };
}
