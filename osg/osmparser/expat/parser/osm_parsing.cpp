/*
* Licensed under the MIT license:
*
* Copyright (c) 2020 Manuel Streicher

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*
*/

#include "osm_parsing.h"

namespace tuk::osg::osmparser::expatparser::parsing
{
    void parse(const char *filename, void *userData, XML_StartElementHandler start, XML_EndElementHandler end) {
        XML_Parser parser = XML_ParserCreate(nullptr);
        XML_SetUserData(parser, userData);
        XML_SetElementHandler(parser, start, end);

        FILE *fh = fopen(filename, "r");
        if (fh==nullptr) {fputs ("File error",stderr); exit (1);}

        int len;
        char val[1024] = {};
        do {
            len = fread(val, sizeof(char), sizeof(val), fh);
            if(XML_STATUS_ERROR == XML_Parse(parser, val, len, static_cast<int>(len < 1024)))
            {
                int code = XML_GetErrorCode(parser);
                const char *msg = (const char *)XML_ErrorString((XML_Error)code);
                fprintf(stderr, "Parsing error code %d message %s\n", code, msg);
            }
        } while (len == 1024);
    }

    void startElement(void *userData, const XML_Char *name, const char *args[]) {
        auto osmParser = static_cast<OsmParser *>(userData);
        tag_map_t attr {};
        makeTagMap(attr, args);
        switch (osmParser->getStatus()) {
            case(EMPTY_NETWORK):
                osmParser->startElementEmptyNetwork(name, attr);
                break;
            case(NETWORK_KNOWS_NODES_AND_WAYS):
                osmParser->startElementKnownNodesAndWays(name, attr);
                break;
            default:
                throw;
        }
    }

    void endElement(void *userData, const XML_Char *name) {
        auto osmParser = static_cast<OsmParser *>(userData);
        switch (osmParser->getStatus()) {
            case(EMPTY_NETWORK):
                osmParser->endElementEmptyNetwork(name);
                break;
            case(NETWORK_KNOWS_NODES_AND_WAYS):
                osmParser->endElementKnownNodesAndWays(name);
                break;
            default:
                throw;
        }
    }

    void makeTagMap(tag_map_t &attr, const char **args) {
        while(*args) {
            std::string current = *args;
            args++;
            attr[current] = *args;
            args++;
        }
    }

}
