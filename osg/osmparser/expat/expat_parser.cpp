/*
* Licensed under the MIT license:
*
* Copyright (c) 2020 Manuel Streicher

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*
*/

#include "expat_parser.h"

namespace tuk::osg::osmparser::expatparser
{
  ExpatParser::ExpatParser(const std::string & filename, const std::unordered_set<std::string> &highwayTypes,
                           const std::list<std::array<std::string, 2>> &kvNodePairs)
  : OSMParser (filename, highwayTypes, kvNodePairs)
  {}

  void ExpatParser::parse()
  {
    auto parser (new OsmParser (*_network, _highwayTypes, _kvNodePairs));
    parser->setStatus (EMPTY_NETWORK);

    parsing::parse (_osmfile.c_str(), parser, parsing::startElement, parsing::endElement);
    parser->setStatus (NETWORK_KNOWS_NODES_AND_WAYS);

    parsing::parse (_osmfile.c_str(), parser, parsing::startElement, parsing::endElement);
    parser->setStatus (NETWORK_COMPLETE);
    delete parser;
  }
}
