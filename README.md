# OpenStreetGraph
![badge](https://img.shields.io/badge/license-MIT-blue)

OpenStreetGraph is a cpp command line tool that, given an [osm xml file](https://wiki.openstreetmap.org/wiki/OSM_XML), creates a [geojson](https://de.wikipedia.org/wiki/GeoJSON) file that represents the street network of the input file as a graph datastructure that permits direct application of shortest path and other graph algorithms.

## Highlights
* Fast xml sax parsing thanks to the [expat](https://libexpat.github.io/) parser (This enables the user to create street graphs of larger areas of the map: A single federal state of Germany such as Rhineland-Palatinate is not a problem.)
* Inclusion and adaption of turning rules making fast turning-restriction respecting routing possible
* Very versatile user options making it possible to include or exclude certain osm-labels and to influence the merging and splitting of osm-ways.

## Using the Tool
In order to get osm xml files of the desired area, try [geofabrik](https://download.geofabrik.de/) for downloading osm map data and [osmosis](https://wiki.openstreetmap.org/wiki/Osmosis) for further adjustments prior street graph creation.

Once you have a viable osm xml file, say input.osm, you can run the tool from the command line using the provided binaries (or self compiled ones):

    create_osgraph input.osm output.geojson
 
 The first argument is always the inputfilename and the second argument is always the output filename. Both arguments are mandatory.
 After the two mandatory arguments the following options are accepted in arbitrary order (some of which require additional arguments):
 
    -nm
If this option is chosen all osm-nodes are kept as vertices in the graph, even if they are not relevant to the graph structure (mostly degree 2 vertices).

    -ka
If this option is chosen, all streets are kept in the resulting street graph, even if they are not connected to the largest strongly connected component. If the option is ommited, only the largest strongly connected component of the street graph is returned.

    -bp bounding_polygon.geojson
The option -bp needs an additional argument directly following the option. The argument should be a filename of a geojson feature collection that contains one or more polygons (coordinates in wgs-84). All vertices that are not contained in any of the provided polygons are removed from the street network. Note that this is always done before computing the largest strongly connected component. An example for a bounding polygon geojson can be found in examples/test_polygon.geojson.

    -skt maxspeed further_tags
The option -skt is assumed to be followed by at least one and arbitrary many further arguments (until argument list ends or next option is detected). Each argument is a osm way tag that is supposed to be transferred to the streets in the network. The osm waytags "oneway" and "type" are always transfered. If this option is omitted, it defaults to only transferring the way tag "maxspeed"

    -ckt highway further_tags
Similarly to the previous option -ckt is assumed to be followed by at least one and arbitrary many further arguments. Each argument is a osm node tag that is supposed to be transferred to the vertices in the network. Note that the tags are only transferred if the corresponding osm node is actually still present in the network. In order to keep all nodes with a certain tag, see option -ckvp.

    -ht motorway motorway_link further_types
Similarly to the previous options -ht is assumed to be followed by at least one and arbitrary many further arguments. Each argument is assumed to be an osm highway type. If the option is present only osm-ways with the provided highway types are considered. If the option is not present, the tool considers the highwaytypes 

    living_street, motorway, motorway_link, primary, primary_link, secondary, secondary_link, tertiary, tertiary_link, residential, trunk, trunk_link, unclassified
Note that the highwaytype "service" is not present in the default settings.

    -ckvp highway:traffic_signals key:value
Again the option -ckvp is assumed to be followed by at least one and arbitrary many further arguments. Each argument is assumed to contain a colon separating key and value of the given argument. All nodes that contain any of the specified key value pairs as tags in the osm xml data are kept in the graph, even if they are not relevant to the graph structure. If the tags should also be present in the street network, this option can be combined with the -ckt option. Note that vertices not contained in the largest strongly component are removed no matter what if the option -ka is not active. If this option is omitted no such pairs are defined.

    -skm maxspeed further_waytags
This option specifies way_tags that ought to be the same for two osm-ways to be merged at a given node (if the node is not relevant to the graph structure). Specifying maxspeed for example, leeds to only merging streets on which the provided maxspeeds coincides. The tag "type" does not need to be specified: two ways of a different type are never merged. If this option is not provided it defaults to -skm maxspeed.

    -of optionfilename.json
If this option is chosen all other options are ignored and the desired options are read from the provided json file. An example for an option file can be found in examples/options_example.json and should be self-explanatory.

A complete example usage with optionfile would be

    create_osgraph input.osm output.geojson -of optionfilename.json
And a complete example without optionfile would be

    create_osgraph input.osm output.geojson -ka -skm maxspeed lanes -ht motorway motorway_link trunk trunk_link

The outputfile format is a geojson Feature Collection. Each feature of the collection has an entry "osm_type" in its properties whoose value is either "crossing", "street" or "restriction". The crossings are the vertices of the graph, the streets are the edges. The restrictions correspond to the turning restrictions within the network. Further, each feature has an id which is unique within its osm_type group. The ids of the crossings correspond to the ids of the osm-nodes the crossings arise from. Every crossing feature is a Point and has no additional mandatory properties other than "osm_type". Every street feature is a LineString and, additionaly, has the property entries "start", "end", "oneway", "length_in_m", "origin" and "type". The values in "start" and "end" correspond to the id of the startcrossing (endcrossing) of the street. In particular, the first coordinate of the LineString corresponds to the coordinate of the startcrossing and the last coordinate to that of the endcrossing. "oneway" can be true or false. If true the street is only directed from start to end, otherwise there exist edges in both directions. "length_in_m" specifies the length of the street in meter. "origin" contains a list of osm way-ids from which the street was created. "type" supplies the osm-highwaytype of the osm-way(s) the street was created from.
Every restriction feature is a LineString and has the additional property entries "restriction_type" and "restriction_elements". "restriction_type" is either "no" or "only". If it is "no" this means that the elements in "restriction_elements" may not be traversed in this exact order (all subtraversals are fine). If it is "only" this means that if the elements in "restriction_elements" are traversed in that exact order until the penultimate crossing id, then the only possible next street to be driven is the one following the penultimate crossing id. The elements are alternately crossing and street ids and start and end with a crossing id.

## License
The tool is licensed under the MIT license.

## Contact and Contributing
Any questions or contributing ideas? Feel free to contact me [manuel.streicher@gmx.net](mailto:manuel.streicher@gmx.net). I also accept feedback of any kind ;)
