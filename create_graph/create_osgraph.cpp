/*
 * Licensed under the MIT license:
 *
 * Copyright (c) 2020 Manuel Streicher

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 *
 */

#include <iostream>
#include <time.h>
#include <filesystem>

#include "../osg/osmparser/expat/expat_parser.h"
#include "../osg/osgraph/osgraph.h"
#include "../osg/graph_algorithms/components.h"

using namespace tuk::osg;

int main(int argc, char *argv[]) {
    clock_t t;
    std::unique_ptr<osgraph::OSGraph> graph;
    {
        std::filesystem::path filename;
        std::filesystem::path outfilename;
        if (argc < 3) {
            std::cout << "No input and outputfile chosen. First and second argument need to specify input and "
                         "output filename." << std::endl;
            return 0;
        } else {

            std::stringstream convert{argv[1]};
            filename = convert.str();
            std::cout << "input filename choice: " << filename << std::endl;
            std::stringstream convert2{argv[2]};
            outfilename = convert2.str();
            std::cout << "output filename choice: " << outfilename << std::endl;
        }
        if (outfilename.extension() != ".json" && outfilename.extension() != ".geojson") {
            std::cout << "Supplied file extension " << outfilename.extension() << " is currently not supported by"
                      << " the osg-creator." << std::endl;
            return 0;
        }
        if (!std::filesystem::exists(filename)) {
            std::cout << "Chosen inputfile does not exist." << std::endl;
            return 0;
        }
        if (!outfilename.parent_path().empty() && !std::filesystem::exists(outfilename.parent_path())) {
            std::cout << "Filepath for outputfilename does not exist." << std::endl;
            return 0;
        }
        std::string optionstyle;
        bool simplify{true}, connected{true}, bounding_polygon{false}, ud_highwaytypes{false}, ud_crossingKeys{false};
        bool ud_streetKeys{false}, ud_streetKeysForEqual{false}, ud_crossingKeyValueParis{false};
        std::filesystem::path bounding_polygon_fn;
        std::unordered_set<std::string> highwayTypes, crossingKeys, streetKeys, streetKeysForEqual;
        std::list<std::array<std::string, 2>> crossingKeyValuePairs;
        int ca {3};
        while (argc > ca) {
            std::stringstream convert{argv[ca]};
            convert >> optionstyle;
            if (optionstyle == "-of") {

                std::string optionfilename {argv[ca + 1]};
                if (!std::filesystem::exists(optionfilename)) {
                    std::cout << "Chosen optionfilename does not exist." << std::endl;
                    return 0;
                } else {
                    std::cout << "Options are being read from optionfile " << optionfilename << std::endl;
                    std::cout << "All manually entered options are ignored." << std::endl;
                }
                std::ifstream optionfile {optionfilename};
                Json::Value root;
                optionfile >> root;
                simplify = root[S_SIMPLIFY].asBool();
                connected = root[S_STRONG_CONNECT].asBool();
                bounding_polygon_fn = root[S_BOUNDING_POLYGON].asString();
                std::cout << "bounding polygon filename: " << bounding_polygon_fn << std::endl;
                streetKeys.clear();
                for (const auto & elt : root[S_STREET_KEYS]) {
                    streetKeys.insert(elt.asString());
                }
                crossingKeys.clear();
                for (const auto & elt : root[S_CROSSING_KEYS]) {
                    crossingKeys.insert(elt.asString());
                }
                highwayTypes.clear();
                for (const auto & elt : root[S_HIGHWAY_TYPES]) {
                    highwayTypes.insert(elt.asString());
                }
                streetKeysForEqual.clear();
                for (const auto & elt : root[S_STREET_VALUES_SAME]) {
                    streetKeysForEqual.insert(elt.asString());
                }
                crossingKeyValuePairs.clear();
                for (auto & elt : root[S_CROSSING_KV_PAIRS_CONSERVE]) {
                    //Json::Value elt {root[S_STREET_]}
                    crossingKeyValuePairs.push_back({elt[0].asString(), elt[1].asString()});
                }
                ca += 2;
                ud_streetKeysForEqual = true;
                ud_crossingKeys = true;
                ud_highwaytypes = true;
                ud_streetKeys = true;
                ud_crossingKeyValueParis = true;
                break;

            } else if (optionstyle == "-nm") {
                simplify = false;
                std::cout << "Nomerge option chosen: Same-labeled streets will not be merged at degree two vertices."
                        << std::endl;
                ++ca;
            } else if (optionstyle == "-ka") {
                connected = false;
                std::cout
                        << "keep all option chosen: The complete graph is computed, not only the largest strongly "
                           "connected component."
                        << std::endl;
                ++ca;
            } else if (optionstyle == "-bp") {
                bounding_polygon = true;
                std::stringstream convert3{argv[ca + 1]};
                bounding_polygon_fn = convert3.str();

                if (!std::filesystem::exists(bounding_polygon_fn)) {
                    std::cout << "Chosen bounding polygon filename " << bounding_polygon_fn << " does not exist." << std::endl;
                    return 0;
                }
                std::cout << "Chose bounding polygon " << bounding_polygon_fn << std::endl;
                ca += 2;

            } else if (optionstyle == "-skt"){
                ud_streetKeys = true;
                ++ca;
                if (ca >= argc) break;
                std::string c_street_key {argv[ca]};
                while (c_street_key.find('-') != 0) {
                    streetKeys.insert(std::move(c_street_key));
                    ++ca;
                    if (ca >= argc) break;
                    std::stringstream convert3{argv[ca]};
                    c_street_key = convert3.str();
                }
            } else if (optionstyle == "-ht") {
                ud_highwaytypes = true;
                ++ca;
                if (ca >= argc) break;
                std::string c_street_type {argv[ca]};
                while (c_street_type.find('-') != 0) {
                    highwayTypes.insert(std::move(c_street_type));
                    ++ca;
                    if (ca >= argc) break;
                    std::stringstream convert3{argv[ca]};
                    c_street_type = convert3.str();
                }
            } else if (optionstyle == "-ckt") {
                ud_crossingKeys = true;
                ++ca;
                if (ca >= argc) break;
                std::string c_street_type {argv[ca]};
                while (c_street_type.find('-') != 0) {
                    crossingKeys.insert(std::move(c_street_type));
                    ++ca;
                    if (ca >= argc) break;
                    std::stringstream convert3{argv[ca]};
                    c_street_type = convert3.str();
                }
            } else if (optionstyle == "-ckvp") {
                ud_crossingKeyValueParis = true;
                ++ca;
                if (ca >= argc) break;
                std::string s {argv[ca]};
                while (s.find('-') != 0) {
                    crossingKeyValuePairs.push_back({s.substr(0, s.find(':')),
                                                     s.substr(s.find(':') + 1)});
                    ++ca;
                    if (ca >= argc) break;
                    std::stringstream convert3{argv[ca]};
                    s = convert3.str();
                }
            } else if (optionstyle == "-skm") {
                ud_streetKeysForEqual = true;
                ++ca;
                if (ca >= argc) break;
                std::string c_street_type {argv[ca]};
                while (c_street_type.find('-') != 0) {
                    streetKeysForEqual.insert(std::move(c_street_type));
                    ++ca;
                    if (ca >= argc) break;
                    std::stringstream convert3{argv[ca]};
                    c_street_type = convert3.str();
                }
            } else {
                std::cout << "Invalid option " << optionstyle << " entered." << std::endl;
            }
        }
        if (!ud_streetKeys) streetKeys.insert(STREET_KEYS.begin(), STREET_KEYS.end());
        if (!ud_highwaytypes) highwayTypes.insert(HIGHWAY_TYPES.begin(), HIGHWAY_TYPES.end());
        if (!ud_streetKeysForEqual) streetKeysForEqual.insert(STREET_KEYS.begin(), STREET_KEYS.end());
        if (!ud_crossingKeys) crossingKeys.insert(CROSSING_KEYS.begin(), CROSSING_KEYS.end());
        if (!ud_crossingKeyValueParis) {
            for (const auto & elt : CROSSING_KV_PAIRS_CONSERVE) {
                crossingKeyValuePairs.push_back(elt);
            }
        }


        std::cout << "The following osm-highwaytypes will be included in the graph: " << std::endl;
        int counter = 0;
        for (const auto &htype : highwayTypes) {
            counter += 1;
            std::cout << htype;
            if (counter == highwayTypes.size()) std::cout << std::endl;
            else std::cout << ", ";
        }

        if (!streetKeys.empty()) {
            std::cout << "The following additional way labels will be transfered to the osg streets: " << std::endl;
            counter = 0;
            for (const auto &elt : streetKeys) {
                counter += 1;
                std::cout << elt;
                if (counter == streetKeys.size()) std::cout << std::endl;
                else std::cout << ", ";
            }
        }

        if (!crossingKeys.empty()) {
            std::cout << "The following additional node labels will be transfered to the osg crossings: " << std::endl;
            counter = 0;
            for (const auto &elt : crossingKeys) {
                counter += 1;
                std::cout << elt;
                if (counter == crossingKeys.size()) std::cout << std::endl;
                else std::cout << ", ";
            }
        }

        if (!streetKeysForEqual.empty()) {
            std::cout << "The following street labels need the same value in order to be merged if simplify is active. " << std::endl;
            counter = 0;
            for (const auto &elt : streetKeysForEqual) {
                counter += 1;
                std::cout << elt;
                if (counter == streetKeysForEqual.size()) std::cout << std::endl;
                else std::cout << ", ";
            }
        }

        if (!crossingKeyValuePairs.empty()) {
            std::cout << "If any of the following key value pairs is present in a osm node, the crossing is not merged, even if it has degree 2." << std::endl;
            counter = 0;
            for (const auto &elt : crossingKeyValuePairs) {
                counter += 1;
                std::cout << "(" << elt[0] << ", " << elt[1] << ")";
                if (counter == crossingKeyValuePairs.size()) std::cout << std::endl;
                else std::cout << ", ";
            }
        }

        osmparser::expatparser::ExpatParser parser(filename.string(), highwayTypes, crossingKeyValuePairs);

        t = clock();
        std::cout << "Parsing the osm file..." << std::endl;
        parser.parse();
        t = clock() - t;
        std::cout << "Parsing process completed in "
                  << ((float) t) / CLOCKS_PER_SEC
                  << "s."
                  << std::endl;

        std::shared_ptr<rawNetwork::RawOsmNetwork> raw_osm_network(parser.network());
        std::cout << "The raw-osm-network has "
                  << raw_osm_network->number_nodes() << " nodes, "
                  << raw_osm_network->number_ways() << " ways, and "
                  << raw_osm_network->number_restrictions() << " restrictions."
                  << std::endl;
        std::cout << "Creating the actual graph..." << std::endl;

        t = clock();
        graph = std::make_unique<osgraph::OSGraph>(*raw_osm_network, crossingKeyValuePairs);
        t = clock() - t;
        std::cout << "Graph creation completed in " << ((float) t) / CLOCKS_PER_SEC << "s." << std::endl;
        std::cout << "The created OpenStreetGraph has "
                  << graph->number_of_nodes() << " crossings, "
                  << graph->number_of_streets() << " streets, and "
                  << graph->number_of_restrictions() << " restrictions."
                  << std::endl;
//        for (auto & street : graph->streets()) {
//            if (!(graph->containsEdge(street.second->start(), street.second->end(), street.second))) {
//                std::cout << "Graph does not contain edge from " << street.second->start().id() << " to "
//                            << street.second->end().id() << std::endl;
//                if (!street.second->oneway() && !(graph->containsEdge(street.second->end(), street.second->start(), street.second))) {
//                    std::cout << "Graph does not contain edge from " << street.second->end().id() << " to "
//                              << street.second->start().id() << std::endl;
//                }
//            }
//        }

        if (bounding_polygon) {
            std::cout << "Removing graph elements that are not contained in bounding polygons..." << std::endl;
            t = clock();
            std::ifstream polygon_f {bounding_polygon_fn};
            Json::Value root;
            polygon_f >> root;
            std::list<std::list<coord_pair_t>> bounding_polygons;
            for (Json::Value::ArrayIndex i = 0; i != root[osgraph::geojson::FEATURES].size(); i++) {
                Json::Value feature = root[osgraph::geojson::FEATURES][i];
                if (feature[osgraph::geojson::GEOMETRY][osgraph::geojson::TYPE] == osgraph::geojson::POLYGON) {
                    std::list<coord_pair_t> poly;
                    for (auto & coordlists : feature[osgraph::geojson::GEOMETRY][osgraph::geojson::COORDINATES]) {
                        for (auto & coord : coordlists) {
                            poly.push_back({coord[0].asDouble(), coord[1].asDouble()});
                        }
                        break;
                    }
                    bounding_polygons.push_back(poly);
                } else if (feature[osgraph::geojson::GEOMETRY][osgraph::geojson::TYPE] == osgraph::geojson::MULTI_POLYGON) {
                    for (auto & polygon : feature[osgraph::geojson::GEOMETRY][osgraph::geojson::COORDINATES]) {
                        std::list<coord_pair_t> poly;
                        for (auto &coordlists : feature[osgraph::geojson::GEOMETRY][osgraph::geojson::COORDINATES]) {
                            for (auto &coord : coordlists) {
                                poly.push_back({coord[0].asDouble(), coord[1].asDouble()});
                            }
                            break;
                        }
                        bounding_polygons.push_back(poly);
                    }
                }
            }
            t = clock() - t;
            std::cout << "Creating polyhedra took " << ((float) t) / CLOCKS_PER_SEC << "s." << std::endl;
            t = clock();
            graph->remove_streets_outside_bounds(bounding_polygons);
            t = clock() - t;
            std::cout << "Removed graph elements in " << ((float) t) / CLOCKS_PER_SEC << "s." << std::endl;
            std::cout << "The created OpenStreetGraph has "
                      << graph->number_of_nodes() << " crossings, "
                      << graph->number_of_streets() << " streets, and "
                      << graph->number_of_restrictions() << " restrictions "
                      << "after removing the elements."
                      << std::endl;
        }

        if (connected) {
            std::cout << "Computing strongly connected components..." << std::endl;
            t = clock();
            auto *alg = new StrongConnectionAlgorithm(*graph);
            std::list<std::list<std::weak_ptr<osgraph::OSNode>>> components = alg->run();
            delete alg;
            alg = nullptr;
            t = clock() - t;
            std::cout << "Strongly connected components computed in " << ((float) t) / CLOCKS_PER_SEC << "s."
                      << std::endl;
            std::cout << "The graph has " << components.size() << " components, and the largest component has ";
            size_t largestComp = 0;
            for (const auto &comp : components) {
                largestComp = comp.size() > largestComp ? comp.size() : largestComp;
            }
            std::cout << largestComp << " vertices." << std::endl;
            if (largestComp > 0) {
                std::cout << "Removing vertices not contained in largest component..." << std::endl;
                t = clock();
                graph->only_keep_largest_component(components);
                t = clock() - t;
                std::cout << "Removed vertices in " << ((float) t) / CLOCKS_PER_SEC << "s." << std::endl;
                std::cout << "The created OpenStreetGraph has "
                          << graph->number_of_nodes() << " crossings, "
                          << graph->number_of_streets() << " streets, and "
                          << graph->number_of_restrictions() << " restrictions "
                          << "after removing vertices."
                          << std::endl;
            }
        }
        if (simplify) {

            std::cout << "Smoothing out non-essential degree two vertices incident with same taged streets..."
                      << std::endl;
            t = clock();
            graph->mergeDegreeTwoCrossings(streetKeysForEqual);
            t = clock() - t;
            std::cout << "Smoothing completed in " << ((float) t) / CLOCKS_PER_SEC << "s." << std::endl;
            std::cout << "The created OpenStreetGraph has "
                      << graph->number_of_nodes() << " crossings, "
                      << graph->number_of_streets() << " streets, and "
                      << graph->number_of_restrictions() << " restrictions "
                      << "after smoothing out."
                      << std::endl;
        }


        std::cout << "Writing graph to file..." << std::endl;
        if (outfilename.extension() == ".json") {
            t = clock();
            //graph->writeGraphToJson("resources/street_graph_files/" + filename + ".json");
            graph->writeGraphToJson(outfilename.string(), streetKeys, crossingKeys);
            t = clock() - t;
            std::cout << "Writing completed in " << ((float) t) / CLOCKS_PER_SEC << "s." << std::endl;
        } else if (outfilename.extension() == ".geojson") {
            t = clock();
            //graph->writeGraphToGeoJson("resources/street_graph_files/" + filename + ".geojson");
            graph->writeGraphToGeoJson(outfilename.string(), streetKeys, crossingKeys);
            t = clock() - t;
            std::cout << "Writing completed in " << ((float) t) / CLOCKS_PER_SEC << "s." << std::endl;
        } else {
            std::cout << "Supplied file extension " << outfilename.extension() << " is currently not supported by"
                      << " the osg-creator." << std::endl;
        }

    }


}
