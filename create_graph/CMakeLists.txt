add_executable(create_osgraph create_osgraph.cpp)
target_link_libraries (create_osgraph osg-osgraph-osgraph)
target_link_libraries (create_osgraph osg-algorithms-components)
target_link_libraries (create_osgraph osg-osmparser-expat)
target_link_libraries (create_osgraph osg-raw_osm_network-raw_osm_network)
